Simple command-line application developed for testing the flux reconstruction method for inviscid compressible twodimensional flows (Euler equations).

Developed during my PhD studies.

As it is a personal project never meant to be presented, I paid little to no attention to clear git history, meaningful commit names, testing, CI/CD etc. Also the code itself is not something one can be proud of, as the most important was to implement the algorithms very quickly to allow for running the simulations as soon as possible, and so code quality was a bit compromised.

Dead for a long time, useful only as a representation of my programming skills back then (2020 - 2022).