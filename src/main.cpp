#include <chrono>
#include <filesystem>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <optional>
#include <string_view>

#include "border.h"
#include "borderValues2D.h"
#include "boundaryConditions.h"
#include "computeDeltaT.h"
#include "computeResiduum.h"
#include "correctionFunction.h"
#include "discontinuousFluxDerivative.h"
#include "dualFaceValues2D.h"
#include "faceValues2D.h"
#include "hllc.h"
#include "initialCondition.h"
#include "lagrangePolynomial.h"
#include "legendrePolynomial.h"
#include "mesh2D.h"
#include "myVector.h"
#include "pointValues2D.h"
#include "points.h"
#include "spatialDerivative.h"
#include "transformValues.h"
#include "utility.h"
#include "utilityFunctions.h"

namespace fs = std::filesystem;

using namespace std::literals::string_view_literals;

int main (int argc, char* argv[])
{
    bool saveResults {};
    
    if (argc >= 2 and argv[1] == "--save"sv)
        saveResults = true;
    
    std::cout << std::setprecision(6) << std::fixed;
    std::cerr << std::setprecision(6) << std::scientific;
    
    int finalIteration {};
    
    std::cout << "Final iteration: ";
    std::cin >> finalIteration;
    
    double t {0.};
    // double deltaT {};
    
    std::string const meshFile {"naca0012grid" + std::to_string(numberOfElementsX) + "x" + std::to_string(numberOfElementsY) + ".dat"};
    
    constexpr auto points {getPoints<polynomialOrder>()};
    
    // for each face (boundary faces included)
    // dualFaceValues2D<Vector4D, polynomialOrder> faceUValues(numberOfElementsX, numberOfElementsY);
    
    // physical borderValues
    // borderValues2D<vector4D, polynomialOrder> leftBorderFaceValues(numberOfElementsY);
    // borderValues2D<vector4D, polynomialOrder> rightBorderFaceValues(numberOfElementsY);
    // borderValues2D<vector4D, polynomialOrder> topBorderFaceValues(numberOfElementsX);
    // borderValues2D<vector4D, polynomialOrder> bottomBorderFaceValues(numberOfElementsX);
    
    pointValues2D<vector4D, polynomialOrder> pointUValues(numberOfElementsX, numberOfElementsY);
    pointValues2D<vector4D, polynomialOrder> newPointUValues(numberOfElementsX, numberOfElementsY);
    pointValues2D<vector4D, polynomialOrder> pointUValuesPhysical(numberOfElementsX, numberOfElementsY);
    
    pointValues2D<double, polynomialOrder> timeSteps(numberOfElementsX, numberOfElementsY);
    
    std::optional<pointValues2D<vector4D, polynomialOrder>> intermediatePointUValues;
    intermediatePointUValues.emplace(numberOfElementsX, numberOfElementsY);
    
    pointValues2D<vector4D, polynomialOrder> pointFluxFValues(numberOfElementsX, numberOfElementsY);
    pointValues2D<vector4D, polynomialOrder> pointFluxGValues(numberOfElementsX, numberOfElementsY);
    
    pointValues2D<vector4D, polynomialOrder> jacobians (numberOfElementsX, numberOfElementsY);
    
    double const uMax = 2.;
    double const uMin = 0.;
    double const vMax = 2.;
    double const vMin = 0.;
    
    auto identityFunction = [](double u, double v) -> point2D
    {
        return point2D(u, v);
    };
    
    auto squareFunction = [](double u, double v) -> point2D
    {
        return point2D(u, v * v * v / 4.);
    };
    
    auto squareRootFunction = [](double x, double y) -> point2D
    {
        return point2D(x, std::pow(y, 1. / 3.) / 4.);
    };
    
    auto leftFunction = squareFunction;
    auto leftInverseFunction = squareRootFunction;
    // auto leftDerivative = identityFunction;
    auto leftDerivative = [](double u, double v) -> point2D
    {
        return point2D(-1, 0);
    };
    
    auto rightFunction = squareFunction;
    auto rightInverseFunction = squareRootFunction;
    // auto rightDerivative = identityFunction;
    auto rightDerivative = [](double u, double v) -> point2D
    {
        return point2D(1, 0);
    };
    
    auto topFunction = identityFunction;
    auto topInverseFunction = identityFunction;
    // auto topDerivative = identityFunction;
    auto topDerivative = [](double u, double v) -> point2D
    {
        return point2D(0, 1);
    };
    
    auto bottomFunction = [](double u, double v) -> point2D
    {
        // if (u > 1 and u < 2)
            // return point2D(u, std::sqrt(-u * u + 3 * u - 0.56) - 1.2);
        // else
            return point2D(u, v);
    };
    auto bottomInverseFunction = [](double x, double y) -> point2D
    {
        return point2D(x, 0.);
    };
    auto bottomDerivative = [](double u, double v) -> point2D
    {
        // if (u > 1 and u < 2)
            // return point2D((-2 * u + 3) / (2 * std::sqrt(-u * u + 3 * u - 0.56)), -1);
        // else
            return point2D(0, -1);
    };
    
    border leftBorder(std::move(leftFunction), std::move(leftInverseFunction), std::move(leftDerivative), uMin, uMin, vMin, vMax);
    border rightBorder(std::move(rightFunction), std::move(rightInverseFunction), std::move(rightDerivative), uMax, uMax, vMin, vMax);
    border topBorder(std::move(topFunction), std::move(topInverseFunction), std::move(topDerivative), uMin, uMax, vMax, vMax);
    border bottomBorder(std::move(bottomFunction), std::move(bottomInverseFunction), std::move(bottomDerivative), uMin, uMax, vMin, vMin);
    
    mesh2D mesh(numberOfElementsX, numberOfElementsY);
    
    mesh.setBorders(leftBorder, rightBorder, topBorder, bottomBorder);
    
    if (not mesh.createMesh(true, meshFile))
        return 0;
    
    initialCondition(pointUValuesPhysical, numberOfElementsX, numberOfElementsY, mesh);
    
    transformValues<false>(pointUValuesPhysical, pointUValues, mesh);
    
    std::vector<std::pair<double, double>> convergence;
    convergence.reserve(finalIteration / 10);
    
    int iteration {};
    
    bool run {true};
    
    if (finalIteration == 0)
        run = false;
    
    auto startTime {std::chrono::steady_clock::now()};
    
    while (run)
    {
        // deltaT = computeDeltaT(pointUValues, mesh, jacobians, CFL, viscous);
        computeDeltaT(pointUValues, mesh, timeSteps, CFL, viscous);
        // t += deltaT;
        
        iteration++;
        
        intermediatePointUValues.value() = pointUValues - /*deltaT **/ spatialDerivative(pointUValues, pointFluxFValues, pointFluxGValues, timeSteps, mesh, viscous);
        
        intermediatePointUValues.value() = (3. / 4.) * pointUValues + (1. / 4.) * intermediatePointUValues.value() -
            (1. / 4.) * /*deltaT **/ spatialDerivative(intermediatePointUValues.value(), pointFluxFValues, pointFluxGValues, timeSteps, mesh, viscous);
        newPointUValues = (1. / 3.) * pointUValues + (2. / 3.) * intermediatePointUValues.value() -
            (2. / 3.) * /*deltaT **/ spatialDerivative(intermediatePointUValues.value(), pointFluxFValues, pointFluxGValues, timeSteps, mesh, viscous);
        
        auto const residuum {computeResiduum(pointUValues, newPointUValues, timeSteps)};
        
        if (std::isnan(residuum))
        {
            std::cout << "Calculation diverged\n";
            
            return 0;
        }
        
        if (/*iteration < 100 or*/ iteration % 10 == 0)
            std::cout << "Iteration = " << iteration << " Time = " << t << /*" deltaT = " << deltaT <<*/ " residuum " << residuum << '\n';
        
        if (iteration % 10 == 0)
            convergence.push_back({iteration, residuum});
        
        if (residuum < -10)
            run = false;
        
        pointUValues = newPointUValues;
        
        if (iteration >= finalIteration)
            run = false;
    }
    
    auto const endTime {std::chrono::steady_clock::now()};
    std::chrono::duration<double> const elapsedSeconds {endTime - startTime};
    
    std::cout << "elapsed time: " << elapsedSeconds.count() << "\n";
    
    fs::path caseDirectory;
    auto iCase {0};
    
    do
    {
        caseDirectory = "./case_" + std::to_string(iCase);
        
        iCase++;
    }
    while (fs::exists(caseDirectory));

    if (saveResults)
    {
        fs::create_directory(caseDirectory);
    }
    
    std::ofstream resultsStream;
    if (saveResults)
    {
        resultsStream.open(caseDirectory / "results.dat");
        resultsStream << std::setprecision(6) << std::fixed;
    }
    
    transformValues<true>(pointUValuesPhysical, pointUValues, mesh);
    
    double liftCoefficient;
    double dragCoefficient;
    double entropyErrorNorm {0.};
    
    if (saveResults)
    {
        resultsStream << "TITLE = \"Results\"\n"
                      << "VARIABLES = \"X\", \"Y\", \"rho\", \"u\", \"v\", \"p\", \"M\", \"s\", \"e\"\n"
                      << "ZONE T = \"BIG ZONE\", I = \"" <<
            numberOfElementsX * (polynomialOrder + 1) << "\", J = \"" <<
            numberOfElementsY * (polynomialOrder + 1) + 1 << "\", F = \"POINT\"\n";
        
        point2D X;
        vector4D result;
        
        for (unsigned iLineY = 0; iLineY <= numberOfElementsY * (polynomialOrder + 1); iLineY++)
        {
            for (unsigned iLineX = 0; iLineX <= numberOfElementsX * (polynomialOrder + 1) - 1; iLineX++)
            {
                // interpolation to wall
                if (iLineY == numberOfElementsY * (polynomialOrder + 1))
                {
                    auto const iElementX {iLineX / (polynomialOrder + 1)};
                    auto const jPointX {iLineX % (polynomialOrder + 1)};
                    
                    // TODO average on interface
                    
                    X = mesh.getCoordinate(iElementX, numberOfElementsY - 1, points[jPointX], 1.);
                    
                    result = lagrangePolynomial<polynomialOrder>::interpolateToRightEdge(pointUValues.values1DVertical(iElementX, numberOfElementsY - 1, jPointX));
                    
                    auto const jacobian {mesh.getJacobian(iElementX, numberOfElementsY - 1, points[jPointX], 1.)};
                    
                    result = transformValue<true>(result, jacobian);
                }
                else
                {
                    auto const iElementX {iLineX / (polynomialOrder + 1)};
                    auto const iElementY {iLineY / (polynomialOrder + 1)};
                    
                    auto const jPointX {iLineX % (polynomialOrder + 1)};
                    auto const jPointY {iLineY % (polynomialOrder + 1)};
                    
                    X = mesh.getCoordinate(iElementX, iElementY, points[jPointX], points[jPointY]);
                    
                    result = pointUValuesPhysical(iElementX, iElementY, jPointX, jPointY);
                }
                
                auto const rho {result[1]};
                
                resultsStream << X.x() << ' ' << X.y() << ' ' <<
                    result[1] << ' ' <<
                    result[2] / rho << ' ' <<
                    result[3] / rho << ' ' <<
                    thermo::getPressure(result) << ' ' <<
                    thermo::getMachNumber(result) << ' ' <<
                    thermo::getEntropy(result) << ' ' <<
                    result[4] << '\n';
            }
        }
        
        std::ofstream pressureCoefficientTopStream;
        pressureCoefficientTopStream.open(caseDirectory / "cpTop");
        pressureCoefficientTopStream << std::setprecision(6) << std::fixed;
        
        std::ofstream pressureCoefficientBottomStream;
        pressureCoefficientBottomStream.open(caseDirectory / "cpBottom");
        pressureCoefficientBottomStream << std::setprecision(6) << std::fixed;
        
        double liftY {0.};
        double dragX {0.};
        
        for (unsigned iElementX = wallBeginning; iElementX < numberOfElementsX / 2; iElementX++)
        {
            double pAvg {0.};
            
            for (unsigned jPointX = 0; jPointX <= polynomialOrder; jPointX++)
            {
                auto x {mesh.getCoordinate(iElementX, numberOfElementsY - 1, points[jPointX], 1.).x()};
                
                auto result {lagrangePolynomial<polynomialOrder>::interpolateToRightEdge(pointUValues.values1DVertical(iElementX, numberOfElementsY - 1, jPointX))};
                
                auto const jacobian {mesh.getJacobian(iElementX, numberOfElementsY - 1, points[jPointX], 1.)};
                
                result = transformValue<true>(result, jacobian);
                
                auto p {thermo::getPressure(result)};
                
                pressureCoefficientTopStream << x << ' ' << (p - outletPressureRatio * p0) / ((1 - outletPressureRatio) * p0) << '\n';
                
                pAvg += p;
            }
            
            pAvg /= (polynomialOrder + 1);
            
            auto const normalVector {mesh.getNormalVector(iElementX, numberOfElementsY - 1, Face::Top)};
            
            auto const edgeLength {norm(mesh.getCoordinate(iElementX, numberOfElementsY - 1, -1., 1.) -
                                        mesh.getCoordinate(iElementX, numberOfElementsY - 1, 1., 1.))};
            
            liftY += pAvg * edgeLength * normalVector.y();
            dragX += pAvg * edgeLength * normalVector.x();
        }
        
        for (unsigned iElementX = numberOfElementsX / 2; iElementX < wallEnd; iElementX++)
        {
            double pAvg {0.};
            
            for (unsigned jPointX = 0; jPointX <= polynomialOrder; jPointX++)
            {
                auto x {mesh.getCoordinate(iElementX, numberOfElementsY - 1, points[jPointX], 1.).x()};
                
                auto result {lagrangePolynomial<polynomialOrder>::interpolateToRightEdge(pointUValues.values1DVertical(iElementX, numberOfElementsY - 1, jPointX))};
                
                auto const jacobian {mesh.getJacobian(iElementX, numberOfElementsY - 1, points[jPointX], 1.)};
                
                result = transformValue<true>(result, jacobian);
                
                auto p {thermo::getPressure(result)};
                
                pressureCoefficientBottomStream << x << ' ' << (p - outletPressureRatio * p0) / ((1 - outletPressureRatio) * p0) << '\n';
                
                pAvg += p;
            }
            
            pAvg /= (polynomialOrder + 1);
            
            auto const normalVector {mesh.getNormalVector(iElementX, numberOfElementsY - 1, Face::Top)};
            
            auto const edgeLength {norm(mesh.getCoordinate(iElementX, numberOfElementsY - 1, -1., 1.) -
                                        mesh.getCoordinate(iElementX, numberOfElementsY - 1, 1., 1.))};
            
            liftY += pAvg * edgeLength * normalVector.y();
            dragX += pAvg * edgeLength * normalVector.x();
        }
        
        // rotate lift and drag to the correct coordinate system
        auto const lift {liftY * std::cos(AoADeg * std::numbers::pi / 180.) - dragX * std::sin(AoADeg * std::numbers::pi / 180.)};
        auto const drag {dragX * std::cos(AoADeg * std::numbers::pi / 180.) + liftY * std::sin(AoADeg * std::numbers::pi / 180.)};
        
        double const chord {1.};
        
        liftCoefficient = lift / (p0 * (1 - outletPressureRatio) * chord);
        dragCoefficient = drag / (p0 * (1 - outletPressureRatio) * chord);
        
        // entropy computation
        
        auto const rhoInf {rho0 * std::pow(outletPressureRatio, 1. / thermo::kappa())};
        
        auto const entropy {outletPressureRatio * p0 * std::pow(rhoInf, -thermo::kappa())};
        
        auto const weights {getWeights<polynomialOrder>()};
        
        double sum {0.};
        
        for (unsigned iElementX = 0; iElementX < numberOfElementsX; iElementX++)
        {
            for (unsigned iElementY = 0; iElementY < numberOfElementsY; iElementY++)
            {
                for (unsigned jPointX = 0; jPointX < pointUValues.getNumberOfPointsX(iElementX, iElementY); jPointX++)
                {
                    for (unsigned jPointY = 0; jPointY < pointUValues.getNumberOfPointsY(iElementX, iElementY); jPointY++)
                    {
                        auto const s {thermo::getEntropy(pointUValuesPhysical(iElementX, iElementY, jPointX, jPointY))};
                        
                        auto const jacobian {mesh.getJacobian(iElementX, iElementY, points[jPointX], points[jPointY])};
                        
                        auto const entropyError {(entropy - s) / s};
                        
                        sum += (entropyError * entropyError * weights[jPointX] * weights[jPointY] * jacobian);
                    }
                }
            }
        }
        
        entropyErrorNorm = std::sqrt(sum);
    }
    
    if (saveResults)
    {
        std::ofstream convergenceStream;
        convergenceStream.open(caseDirectory / "convergence");

        for (auto const& [iteration, residual] : convergence)
        {
            convergenceStream << iteration << ' ' << residual << '\n';
        }
        
        std::ofstream infoStream;
        infoStream.open(caseDirectory / "info");
        infoStream << std::setprecision(6) << std::fixed;
        
        infoStream <<
            "mesh " << meshFile << '\n' <<
            "elementsX " << numberOfElementsX << '\n' <<
            "elementsY " << numberOfElementsY << '\n' <<
            "polynomial order " << polynomialOrder << '\n' <<
            "CFL " << CFL << '\n' <<
            "iterations " << iteration << '\n' <<
            "elapsed time " << elapsedSeconds.count() << "s\n" <<
            "outlet pressure ratio " << outletPressureRatio << '\n' <<
            "angle of attack " << AoADeg << '\n' <<
            "cl " << std::setprecision(12) << liftCoefficient << '\n' <<
            "cd " << dragCoefficient << '\n' <<
            "L(2) norm of entropy error " << entropyErrorNorm << '\n';
    }

    if (saveResults)
    {
        // std::ofstream velocityProfileStream;

        // velocityProfileStream.open(caseDirectory / "u");
        // velocityProfileStream << std::setprecision(6) << std::fixed;
    }
    
    // for (unsigned iElementX = 0; iElementX < numberOfElementsX; iElementX++)
    // {
    //     for (unsigned jPointX = 0; jPointX <= polynomialOrder; jPointX++)
    //     {
    //         auto vertical1DValues {pointUValues.values1DVertical(iElementX, 0, jPointX)};
            
    //         vector4D UBottom {lagrangePolynomial<polynomialOrder>::interpolate(vertical1DValues, -1.)};
            
    //         double const x {mesh.getCoordinate(iElementX, 0, points[jPointX], -1).x()};
            
    //         double const rho {UBottom[1]};
    //         vector2D const u {UBottom[2] / rho, UBottom[3] / rho};
    //         double const p {thermo::getPressure(UBottom)};
            
    //         double const c {std::sqrt(thermo::kappa() * p / rho)};
            
    //         machStream << x << ' ' << u.norm() / c << '\n';
    //     }
    // }
    
    // velocityProfileStream << 0. << ' ' << 0. << '\n';
    
    // for (unsigned iElementY = 0; iElementY < numberOfElementsY; iElementY++)
    // {
    //     auto vertical1DValues {pointUValues.values1DVertical(39, iElementY, 0)};
        
    //     for (unsigned jPointY = 0; jPointY <= polynomialOrder; jPointY++)
    //     {
    //         double const y {mesh.getCoordinate(16, iElementY, points[0], points[jPointY]).y()};
            
    //         auto U {pointUValuesPhysical(16, iElementY, 0, jPointY)};
            
    //         auto const u {U[2] / U[1]};
            
    //         velocityProfileStream << y << ' ' << u << '\n';
    //     }
    // }
    
    return 0;
}
