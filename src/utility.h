#ifndef UTILITY_H
#define UTILITY_H

#include <array>

enum class Face
{
    Top,
    Bottom,
    Left,
    Right
};

// constexpr auto mu {0.000005};
constexpr auto mu {0.0000185};
constexpr auto Pr {0.71};
constexpr auto kappa {1.4};

auto constexpr p0 {100000.};
auto constexpr rho0 {1.};

auto constexpr outletPressureRatio {0.843};
auto constexpr AoADeg {0.};

// 0.9 0.4 0.2 ...
constexpr double CFL {0.4};
constexpr bool viscous {false};

constexpr unsigned polynomialOrder {1};

constexpr unsigned numberOfElementsX {200};
constexpr unsigned numberOfElementsY {60};
constexpr unsigned wallBeginning {20};
constexpr unsigned wallEnd {180};

//constexpr unsigned numberOfElementsX {200};
//constexpr unsigned numberOfElementsY {30};
//constexpr unsigned wallBeginning {20};
//constexpr unsigned wallEnd {180};

template <unsigned polynomialOrder>
consteval auto getWeights ()
{
    if constexpr (polynomialOrder == 0)
        return std::array<double, 1> {1.};
    else if constexpr (polynomialOrder == 1)
        return std::array<double, 2> {1., 1.};
    else if constexpr (polynomialOrder == 2)
        return std::array<double, 3> {0.5555555555555556, 0.8888888888888888, 0.5555555555555556};
    else if constexpr (polynomialOrder == 3)
        return std::array<double, 4> {0.3478548451374538, 0.6521451548625461, 0.6521451548625461, 0.3478548451374538};
    else if constexpr (polynomialOrder == 4)
        return std::array<double, 5> {0.2369268850561891, 0.4786286704993665, 0.5688888888888889, 0.4786286704993665, 0.2369268850561891};
    else if constexpr (polynomialOrder == 5)
        return std::array<double, 6> {0.1713244923791704, 0.3607615730481386, 0.4679139345726910, 0.4679139345726910, 0.3607615730481386, 0.1713244923791704};
    else if constexpr (polynomialOrder == 6)
        return std::array<double, 7> {0.1294849661688697, 0.2797053914892766, 0.3818300505051189, 0.4179591836734694, 0.3818300505051189, 0.2797053914892766, 0.1294849661688697};
};

#endif
