#ifndef VERTEXFIELD_H
#define VERTEXFIELD_H

#include <iostream>
#include <vector>

enum class Vertex
{
    TopLeft,
    TopRight,
    BottomLeft,
    BottomRight
};

template <typename Type>
class vertexField
{
public:
    
    vertexField (unsigned numberOfElementsX_, unsigned numberOfElementsY_) :
        numberOfElementsX (numberOfElementsX_),
        numberOfElementsY (numberOfElementsY_),
        numberOfElements (numberOfElementsX * numberOfElementsY)
    {
        values.resize((numberOfElementsX + 1) * (numberOfElementsY + 1));
    };
    
    Type& operator() (unsigned iElementX, unsigned jElementY)
    {
        return this->operator()(iElementX, jElementY, Vertex::BottomLeft);
    };
    
    Type const& operator() (unsigned iElementX, unsigned jElementY) const
    {
        return this->operator()(iElementX, jElementY, Vertex::BottomLeft);
    };
    
    Type& operator() (unsigned iElementX, unsigned jElementY, Vertex vertexPosition)
    {
        switch (vertexPosition)
        {
            case Vertex::TopLeft:
                return values[(jElementY + 1) * (numberOfElementsX + 1) + iElementX];
            case Vertex::TopRight:
                return values[(jElementY + 1) * (numberOfElementsX + 1) + iElementX + 1];
            case Vertex::BottomLeft:
                return values[jElementY * (numberOfElementsX + 1) + iElementX];
            case Vertex::BottomRight:
                return values[jElementY * (numberOfElementsX + 1) + iElementX + 1];
            default:
                return values.front();
        }
    };
    
    Type const& operator() (unsigned iElementX, unsigned jElementY, Vertex vertexPosition) const
    {
        switch (vertexPosition)
        {
            case Vertex::TopLeft:
                return values[(jElementY + 1) * (numberOfElementsX + 1) + iElementX];
            case Vertex::TopRight:
                return values[(jElementY + 1) * (numberOfElementsX + 1) + iElementX + 1];
            case Vertex::BottomLeft:
                return values[jElementY * (numberOfElementsX + 1) + iElementX];
            case Vertex::BottomRight:
                return values[jElementY * (numberOfElementsX + 1) + iElementX + 1];
            default:
                return values.front();
        }
    };
    
private:
    
    unsigned numberOfElementsX {};
    unsigned numberOfElementsY {};
    unsigned numberOfElements {};
    
    std::vector<Type> values;
};

#endif
