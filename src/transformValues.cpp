#include "transformValues.h"
#include "myVector.h"

vector4D transformFluxF (vector4D const F, vector4D const G, double xDerivativeEta, double yDerivativeEta)
{
    return yDerivativeEta * F - xDerivativeEta * G; // return transformed value
}

vector4D transformFluxG (vector4D const F, vector4D const G, double xDerivativeXi, double yDerivativeXi)
{
    return xDerivativeXi * G - yDerivativeXi * F; // return transformed value
}
