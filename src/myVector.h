#ifndef MY_VECTOR_H
#define MY_VECTOR_H

#include <array>
#include <ostream>
#include <utility>
#include <type_traits>

template <unsigned size>
class myVector
{
    public:
    
        myVector () {};
        
        //template<typename ...T>
        //myVector(T&&...t) : data{std::forward<T>(t)...} {}
        
        template <unsigned size_ = size, typename = typename std::enable_if<size_ == 2>::type>
        myVector(double a1, double a2) : data{a1, a2} {}
        
        template <unsigned size_ = size, typename = typename std::enable_if<size_ == 2>::type>
        myVector (myVector<2> const a, myVector<2> const b)
        {
            this->x() = a.x() - b.x();
            this->y() = a.y() - b.y();
        }
        
        template <unsigned size_ = size, typename = typename std::enable_if<size_ == 3>::type>
        myVector(double a1, double a2, double a3) : data{a1, a2, a3} {}
        
        template <unsigned size_ = size, typename = typename std::enable_if<size_ == 4>::type>
        myVector(double a1, double a2, double a3, double a4) : data{a1, a2, a3, a4} {}
        
        //myVector (std::initializer_list<double> l) : data{{l}} {};
        
        myVector (const myVector<size>& other) : data(other.data) {}
        ~myVector () {}
        
        double& operator[] (const unsigned index);
        const double& operator[] (const unsigned index) const;
        
        myVector& operator= (const myVector<size>& a);
        myVector& operator+= (const myVector<size>& a);
        myVector& operator-= (const myVector<size>& a);
        myVector& operator*= (const double a);
        myVector& operator/= (const double a);
        myVector& operator*= (const myVector<size>& a);
        
        double norm () const;
        
        void normalize ();
        
        template <unsigned size_ = size>
        typename std::enable_if<size_ == 2 or size_ == 3, double>::type&
        x () { return data[0]; }
        
        template <unsigned size_ = size>
        typename std::enable_if<size_ == 2 or size_ == 3, double>::type&
        y () { return data[1]; }
        
        template <unsigned size_ = size>
        typename std::enable_if<size_ == 3, double>::type&
        z () { return data[2]; }
    
        template <unsigned size_ = size>
        typename std::enable_if<size_ == 2 or size_ == 3, double>::type const&
        x () const { return data[0]; }
        
        template <unsigned size_ = size>
        typename std::enable_if<size_ == 2 or size_ == 3, double>::type const&
        y () const { return data[1]; }
        
        template <unsigned size_ = size>
        typename std::enable_if<size_ == 3, double>::type const&
        z () const { return data[2]; }
        
//    private:
        std::array<double, size> data {};
};

template <unsigned size>
myVector<size> operator+ (const myVector<size>& a, const myVector<size>& b);

template <unsigned size>
myVector<size> operator- (const myVector<size>& a, const myVector<size>& b);

template <unsigned size>
myVector<size> operator* (const double a, const myVector<size>& b);

template <unsigned size>
myVector<size> operator* (const myVector<size>& a, const double b);

template <unsigned size>
myVector<size> operator/ (const myVector<size>& a, const double b);

template <unsigned size>
double operator* (const myVector<size>& a, const myVector<size>& b);

template <unsigned size>
double norm (myVector<size> const& a);

using vector2D = myVector<2u>;
using vector3D = myVector<3u>;
using vector4D = myVector<4u>;
using vector5D = myVector<5u>;

using point2D = myVector<2u>;

vector2D orthoNormal (vector2D other);

std::ostream& operator<< (std::ostream& os, vector2D const& vector);

std::ostream& operator<< (std::ostream& os, vector3D const& vector);

std::ostream& operator<< (std::ostream& os, vector4D const& vector);

#endif
