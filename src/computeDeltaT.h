#ifndef COMPUTE_DELTA_T
#define COMPUTE_DELTA_T

#include <cmath>
#include <vector>

#include "mesh2D.h"
#include "myVector.h"
#include "pointValues2D.h"
#include "points.h"
#include "transformValues.h"

template <unsigned polynomialOrder>
void computeDeltaT (pointValues2D<vector4D, polynomialOrder> const& pointUValues,
                    mesh2D const& mesh, pointValues2D<double, polynomialOrder>& timeSteps,
                    double const CFL, bool const viscous)
{
    // double deltaT {1000.};
    double tempDeltaT {};
    
    auto points {getPoints<polynomialOrder>()};
    
    for (unsigned iElementX = 0; iElementX < mesh.getNumberOfElementsX(); iElementX++)
    {
        for (unsigned iElementY = 0; iElementY < mesh.getNumberOfElementsY(); iElementY++)
        {
            auto const sizeVectorXi {mesh.getSizeVectorXi(iElementX, iElementY)};
            auto const sizeVectorEta {mesh.getSizeVectorEta(iElementX, iElementY)};
            
            for (unsigned jPointX = 0; jPointX < pointUValues.getNumberOfPointsX(iElementX, iElementY); jPointX++)
            {
                for (unsigned jPointY = 0; jPointY < pointUValues.getNumberOfPointsY(iElementX, iElementY); jPointY++)
                {
                    vector4D const UPhysical {transformValue<true>(pointUValues(iElementX, iElementY, jPointX, jPointY), mesh.getJacobian(iElementX, iElementY, points[jPointX], points[jPointY]))};
                    
                    auto const rho {UPhysical[1]};
                    
                    vector2D const u {UPhysical[2] / UPhysical[1], UPhysical[3] / UPhysical[1]};
                    
                    auto const uProjectedXi {u * sizeVectorXi / norm(sizeVectorXi)};
                    auto const uProjectedEta {u * sizeVectorEta / norm(sizeVectorEta)};
                    
                    double const p {thermo::getPressure(UPhysical)};
                    
                    double const c {std::sqrt(thermo::kappa() * p / UPhysical[1])};
                    
                    if (viscous)
                    {
                        tempDeltaT = CFL * (1 / ((std::fabs(uProjectedXi) + c) / norm(sizeVectorXi) + (std::fabs(uProjectedEta) + c) / norm(sizeVectorEta) + 2 * mu / rho * (1 / (norm(sizeVectorXi) * norm(sizeVectorXi)) + 1 / (norm(sizeVectorEta) * norm(sizeVectorEta)))));
                        
                        timeSteps(iElementX, iElementY, jPointX, jPointY) = tempDeltaT;
                    }
                    else
                    {
                        tempDeltaT = CFL * (1 / ((std::fabs(uProjectedXi) + c) / norm(sizeVectorXi) + (std::fabs(uProjectedEta) + c) / norm(sizeVectorEta)));
                        
                        timeSteps(iElementX, iElementY, jPointX, jPointY) = tempDeltaT;
                    }
                    
                    // if (tempDeltaT < deltaT)
                        // deltaT = tempDeltaT;
                }
            }
        }
    }
    
    // return deltaT;
}

#endif
