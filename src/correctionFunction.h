#ifndef CORRECTION_FUNCTION_H
#define CORRECTION_FUNCTION_H

#include <cmath>

#include "legendrePolynomial.h"

template <unsigned polynomialOrder>
class correctionFunction
{
    public:
    
        correctionFunction () = delete;
        
        static constexpr double getLeftDerivativeAtPoint (int const iPoint);
        static constexpr double getRightDerivativeAtPoint (int const iPoint);
        
        static constexpr double getLeftDerivativeAtLeftEdge ();
        static constexpr double getRightDerivativeAtLeftEdge ();
        
        static constexpr double getLeftDerivativeAtRightEdge ();
        static constexpr double getRightDerivativeAtRightEdge ();
};

template <unsigned polynomialOrder>
constexpr double correctionFunction<polynomialOrder>::getLeftDerivativeAtPoint (int const iPoint)
{
    if constexpr (polynomialOrder % 2 == 0)
        return 1. / 2. * (legendrePolynomial<polynomialOrder>::getDerivativeAtPoint(iPoint) - legendrePolynomial<polynomialOrder + 1, polynomialOrder>::getDerivativeAtPoint(iPoint));
    else
        return -1. / 2. * (legendrePolynomial<polynomialOrder>::getDerivativeAtPoint(iPoint) - legendrePolynomial<polynomialOrder + 1, polynomialOrder>::getDerivativeAtPoint(iPoint));
}

template <unsigned polynomialOrder>
constexpr double correctionFunction<polynomialOrder>::getRightDerivativeAtPoint (int const iPoint)
{
    return 1. / 2. * (legendrePolynomial<polynomialOrder>::getDerivativeAtPoint(iPoint) + legendrePolynomial<polynomialOrder + 1, polynomialOrder>::getDerivativeAtPoint(iPoint));
}

template <unsigned polynomialOrder>
constexpr double correctionFunction<polynomialOrder>::getLeftDerivativeAtLeftEdge ()
{
    if constexpr (polynomialOrder % 2 == 0)
        return 1. / 2. * (legendrePolynomial<polynomialOrder>::getDerivativeAtLeftEdge() - legendrePolynomial<polynomialOrder + 1>::getDerivativeAtLeftEdge());
    else
        return -1. / 2. * (legendrePolynomial<polynomialOrder>::getDerivativeAtLeftEdge() - legendrePolynomial<polynomialOrder + 1>::getDerivativeAtLeftEdge());
}

template <unsigned polynomialOrder>
constexpr double correctionFunction<polynomialOrder>::getRightDerivativeAtLeftEdge ()
{
    return 1. / 2. * (legendrePolynomial<polynomialOrder>::getDerivativeAtLeftEdge() + legendrePolynomial<polynomialOrder + 1>::getDerivativeAtLeftEdge());
}

template <unsigned polynomialOrder>
constexpr double correctionFunction<polynomialOrder>::getLeftDerivativeAtRightEdge ()
{
    if constexpr (polynomialOrder % 2 == 0)
        return 1. / 2. * (legendrePolynomial<polynomialOrder>::getDerivativeAtRightEdge() - legendrePolynomial<polynomialOrder + 1>::getDerivativeAtRightEdge());
    else
        return -1. / 2. * (legendrePolynomial<polynomialOrder>::getDerivativeAtRightEdge() - legendrePolynomial<polynomialOrder + 1>::getDerivativeAtRightEdge());
}

template <unsigned polynomialOrder>
constexpr double correctionFunction<polynomialOrder>::getRightDerivativeAtRightEdge ()
{
    return 1. / 2. * (legendrePolynomial<polynomialOrder>::getDerivativeAtRightEdge() + legendrePolynomial<polynomialOrder + 1>::getDerivativeAtRightEdge());
}

#endif
