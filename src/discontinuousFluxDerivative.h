#ifndef DISCONTINUOUS_FLUX_DERIVATIVE_H
#define DISCONTINUOUS_FLUX_DERIVATIVE_H

#include <cmath>

#include "lagrangePolynomial.h"

template <unsigned numericalOrder, typename Type>
constexpr Type factorAtRightEdge (unsigned jPolynomial, Type const fluxValue)
{
    return lagrangePolynomial<numericalOrder>::getDerivativeAtRightEdge(jPolynomial) * fluxValue;
}

template <unsigned numericalOrder, typename Type, template<typename, unsigned> class T, std::size_t... N>
constexpr Type factorsAtRightEdge (T<Type, numericalOrder + 1> const& fluxValues, std::index_sequence<N...>)
{
    return (factorAtRightEdge<numericalOrder>(N, fluxValues[N]) + ...);
}

template <unsigned numericalOrder, typename Type, template<typename, unsigned> class T>
constexpr Type discontinuousFluxDerivativeAtRightEdge (T<Type, numericalOrder + 1> const& fluxValues)
{
    return factorsAtRightEdge<numericalOrder>(fluxValues, std::make_index_sequence<numericalOrder + 1>());
}

template <unsigned numericalOrder, typename Type>
constexpr Type factorAtLeftEdge (unsigned jPolynomial, Type const fluxValue)
{
    return lagrangePolynomial<numericalOrder>::getDerivativeAtLeftEdge(jPolynomial) * fluxValue;
}

template <unsigned numericalOrder, typename Type, template<typename, unsigned> class T, std::size_t... N>
constexpr Type factorsAtLeftEdge (T<Type, numericalOrder + 1> const& fluxValues, std::index_sequence<N...>)
{
    return (factorAtLeftEdge<numericalOrder>(N, fluxValues[N]) + ...);
}

template <unsigned numericalOrder, typename Type, template<typename, unsigned> class T>
constexpr Type discontinuousFluxDerivativeAtLeftEdge (T<Type, numericalOrder + 1> const& fluxValues)
{
    return factorsAtLeftEdge<numericalOrder>(fluxValues, std::make_index_sequence<numericalOrder + 1>());
}

// ------------------------------------------------------------------------------------------------------------------------- //

template <unsigned numericalOrder, typename Type>
constexpr Type factorAtPoint (unsigned jPoint, unsigned iPoint, Type fluxValue)
{
    return lagrangePolynomial<numericalOrder>::getDerivativeAtPoint(jPoint, iPoint) * fluxValue;
}

template <unsigned numericalOrder, typename Type, template<typename, unsigned> class T, std::size_t... N>
constexpr Type factorsAtPoint (T<Type, numericalOrder + 1> const& fluxValues, unsigned const iPoint, std::index_sequence<N...>)
{
    return (factorAtPoint<numericalOrder>(N, iPoint, fluxValues[N]) + ...);
}

template <unsigned numericalOrder, typename Type, template<typename, unsigned> class T>
constexpr Type discontinuousFluxDerivativeAtPoint (T<Type, numericalOrder + 1> const& fluxValues, unsigned const iPoint)
{
    return factorsAtPoint<numericalOrder>(fluxValues, iPoint, std::make_index_sequence<numericalOrder + 1>());
}

#endif
