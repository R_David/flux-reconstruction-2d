#ifndef COMPUTE_RESIDUUM_H
#define COMPUTE_RESIDUUM_H

#include <cmath>

#include "pointValues2D.h"

template <unsigned polynomialOrder>
double computeResiduum (pointValues2D<vector4D, polynomialOrder> const& oldPointUValues,
                        pointValues2D<vector4D, polynomialOrder> const& newPointUValues,
                        pointValues2D<double, polynomialOrder> const& timeSteps)
{
    double sum {};

    for (unsigned iElementX = 0; iElementX < newPointUValues.getNumberOfElementsX(); iElementX++)
    {
        for (unsigned iElementY = 0; iElementY < newPointUValues.getNumberOfElementsY(); iElementY++)
        {
            for (unsigned jPointX = 0; jPointX < newPointUValues.getNumberOfPointsX(iElementX, iElementY); jPointX++)
            {
                for (unsigned jPointY = 0; jPointY < newPointUValues.getNumberOfPointsY(iElementX, iElementY); jPointY++)
                {
                    sum += std::pow((newPointUValues(iElementX, iElementY, jPointX, jPointY)[1] - oldPointUValues(iElementX, iElementY, jPointX, jPointY)[1]) / timeSteps(iElementX, iElementY, jPointX, jPointY), 2.);
                }
            }
        }
    }
    
    return std::log10(std::sqrt(sum));
}

#endif
