#ifndef COMPUTE_FLUX_H
#define COMPUTE_FLUX_H

#include "mesh2D.h"
#include "myVector.h"

vector4D computeFluxF (vector4D UTransformed, mesh2D const& mesh, double xi, double eta, unsigned iElementX, unsigned jElementY);
vector4D computeFluxG (vector4D UTransformed, mesh2D const& mesh, double xi, double eta, unsigned iElementX, unsigned jElementY);

vector4D computeFluxFv (vector4D UTransformed, vector4D QX, vector4D QY, mesh2D const& mesh, double xi, double eta, unsigned iElementX, unsigned jElementY);
vector4D computeFluxGv (vector4D UTransformed, vector4D QX, vector4D QY, mesh2D const& mesh, double xi, double eta, unsigned iElementX, unsigned jElementY);

vector4D computeFluxFvPhysical (vector4D U, vector4D QX, vector4D QY);
vector4D computeFluxGvPhysical (vector4D U, vector4D QX, vector4D QY);

#endif
