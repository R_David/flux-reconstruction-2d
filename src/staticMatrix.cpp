#include "staticMatrix.h"

template <unsigned columns, unsigned rows>
staticMatrix<columns, rows>& staticMatrix<columns, rows>::operator= (staticMatrix<columns, rows> const& other)
{
    data = other.data;
    
    return *this;
}

template <unsigned columns, unsigned rows>
double& staticMatrix<columns, rows>::operator() (unsigned row, unsigned column)
{
    return data[(row - 1) * columns + column - 1];
}

template <unsigned columns, unsigned rows>
double const& staticMatrix<columns, rows>::operator() (unsigned row, unsigned column) const
{
    return data[(row - 1) * columns + column - 1];
}

template <unsigned columns, unsigned rows>
void staticMatrix<columns, rows>::assignRow (int iRow, std::array<double, columns> values)
{
    // TODO
    for (auto iColumn = 1; iColumn <= columns; iColumn++)
        (*this)(iRow, iColumn) = values(iColumn - 1);
}

template <unsigned columns, unsigned rows>
void staticMatrix<columns, rows>::assignColumn (int iColumn, std::array<double, rows> values)
{
    // TODO
    for (auto iRow = 1; iRow <= rows; iRow++)
        (*this)(iRow, iColumn) = values(iRow);
}

template <unsigned columns, unsigned rows>
template <unsigned otherColumns>
staticMatrix<otherColumns, columns> staticMatrix<columns, rows>::operator* (staticMatrix<otherColumns, columns> const& otherMatrix)
{
    staticMatrix<otherColumns, columns> resultMatrix; 
    
    for (unsigned iOtherCol {1}; iOtherCol <= otherColumns; iOtherCol++)
    {
        for (unsigned iRow {1}; iRow <= rows; iRow++)
        {
            double result {0.};
        
            for (unsigned iColumn {1}; iColumn <= columns; iColumn++)
            {
                result += ((*this)(iRow, iColumn) * otherMatrix(iColumn, iOtherCol));
            }
            
            resultMatrix(iRow, iOtherCol) = result;
        }
    }
    
    return resultMatrix;
}

template <unsigned columns, unsigned rows>
staticMatrix<columns, rows> staticMatrix<columns, rows>::operator* (double coefficient)
{
    staticMatrix<columns, rows> result;
    
    for (unsigned iRow {1}; iRow <= rows; iRow++)
    {
        for (unsigned iColumn {1}; iColumn <= columns; iColumn++)
        {
            result(iRow, iColumn) = (*this)(iRow, iColumn) * coefficient;
        }
    }
    
    return result;
}

template <unsigned columns, unsigned rows>
staticMatrix<columns, rows>& staticMatrix<columns, rows>::operator*= (double coefficient)
{
    for (unsigned iRow {1}; iRow <= rows; iRow++)
    {
        for (unsigned iColumn {1}; iColumn <= columns; iColumn++)
        {
            (*this)(iRow, iColumn) = (*this)(iRow, iColumn) * coefficient;
        }
    }
    
    return *this;
}

template <unsigned columns, unsigned rows>
staticMatrix<columns, rows> staticMatrix<columns, rows>::operator/ (double coefficient)
{
    staticMatrix<columns, rows> result;
    
    for (unsigned iRow {1}; iRow <= rows; iRow++)
    {
        for (unsigned iColumn {1}; iColumn <= columns; iColumn++)
        {
            result(iRow, iColumn) = (*this)(iRow, iColumn) / coefficient;
        }
    }
    
    return result;
}

template <unsigned columns, unsigned rows>
staticMatrix<rows, columns> transpose (staticMatrix<columns, rows> matrix)
{
    staticMatrix<rows, columns> result;
    
    for (unsigned iRow {1}; iRow <= rows; iRow++)
    {
        for (unsigned iColumn {1}; iColumn <= columns; iColumn++)
        {
            result(iColumn, iRow) = matrix(iRow, iColumn);
        }
    }
    
    return result;
}

template <unsigned columns, unsigned rows>
vector4D const staticMatrix<columns, rows>::row (unsigned iRow) const
{
    vector4D result;
    
    for (unsigned iCol {1}; iCol <= columns; iCol++)
    {
        result[iCol] = (*this)(iRow, iCol);
    }
    
    return result;
}

template <unsigned columns, unsigned rows>
void staticMatrix<columns, rows>::assignRow (vector4D const& row, unsigned iRow)
{
    for (unsigned iCol {1}; iCol <= columns; iCol++)
    {
        (*this)(iRow, iCol) = row[iCol];
    }
}

template
staticMatrix<2, 2>& staticMatrix<2, 2>::operator= (staticMatrix<2, 2> const& other);

template
staticMatrix<4, 2>& staticMatrix<4, 2>::operator= (staticMatrix<4, 2> const& other);

template
double& staticMatrix<2, 2>::operator() (unsigned row, unsigned column);

template
double const& staticMatrix<2, 2>::operator() (unsigned row, unsigned column) const;

template
double& staticMatrix<4, 2>::operator() (unsigned row, unsigned column);

template
double const& staticMatrix<4, 2>::operator() (unsigned row, unsigned column) const;


template
staticMatrix<2, 2> staticMatrix<2, 2>::operator* (staticMatrix<2, 2> const& otherMatrix);

template
staticMatrix<4, 2> staticMatrix<2, 2>::operator* (staticMatrix<4, 2> const& otherMatrix);

template
staticMatrix<2, 2> staticMatrix<2, 2>::operator* (double coefficient);

template
staticMatrix<4, 2> staticMatrix<4, 2>::operator* (double coefficient);

template
staticMatrix<2, 2>& staticMatrix<2, 2>::operator*= (double coefficient);

template
staticMatrix<4, 2>& staticMatrix<4, 2>::operator*= (double coefficient);

template
staticMatrix<4, 2> staticMatrix<4, 2>::operator/ (double coefficient);

template
staticMatrix<2, 2> transpose (staticMatrix<2, 2> matrix);

template
vector4D const staticMatrix<4, 2>::row (unsigned iRow) const;

template
void staticMatrix<4, 2>::assignRow (vector4D const& row, unsigned iRow);
