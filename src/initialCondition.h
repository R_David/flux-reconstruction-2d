#ifndef INITIAL_CONDITION_H
#define INITIAL_CONDITION_H

#include "points.h"
#include "pointValues2D.h"
#include "myVector.h"

template <unsigned polynomialOrder>
void initialCondition (pointValues2D<vector4D, polynomialOrder>& values, unsigned const numberOfElementsX, unsigned const numberOfElementsY, mesh2D const& mesh)
{
    auto points(getPoints<polynomialOrder>());
    
    for (unsigned iElementX = 0; iElementX < numberOfElementsX; iElementX++)
    {
        for (unsigned iElementY = 0; iElementY < numberOfElementsY; iElementY++)
        {
            for (unsigned jPointX = 0; jPointX < polynomialOrder + 1; jPointX++)
            {
                for (unsigned jPointY = 0; jPointY < polynomialOrder + 1; jPointY++)
                {
                    double const x {mesh.getCoordinate(iElementX, iElementY, points[jPointX], points[jPointY]).x()};
                    
                    double const rho {0.9};
                    
                    vector2D const u {150., 0.};
                    //double const p {100000 - 26300 / 3 * x};
                    double const p {84000.};
                    
                    values(iElementX, iElementY, jPointX, jPointY) = vector4D {rho, rho * u.x(), rho * u.y(), thermo::getEnergy(rho, u, p)};
                }
            }
        }
    }
}

#endif

