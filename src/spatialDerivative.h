#ifndef SPATIAL_DERIVATIVE_H
#define SPATIAL_DERIVATIVE_H

#include "boundaryConditions.h"
#include "computeFlux.h"
#include "correctionFunction.h"
#include "dualFaceValues2D.h"
#include "mesh2D.h"
#include "myVector.h"
#include "pointValues2D.h"
#include "points.h"
#include "staticMatrix.h"
#include "viscousFlux.h"

template <unsigned polynomialOrder>
pointValues2D<vector4D, polynomialOrder> spatialDerivative (pointValues2D<vector4D, polynomialOrder> const& pointUValues,
                                                            pointValues2D<vector4D, polynomialOrder>& pointFluxFValues,
                                                            pointValues2D<vector4D, polynomialOrder>& pointFluxGValues,
                                                            pointValues2D<double, polynomialOrder> const& timeSteps,
                                                            mesh2D const& mesh, bool viscous)
{
    auto const numberOfElementsX {pointUValues.getNumberOfElementsX()};
    auto const numberOfElementsY {pointUValues.getNumberOfElementsY()};
    
    borderValues2D<vector4D, polynomialOrder> leftBorderFaceValues(numberOfElementsY);
    borderValues2D<vector4D, polynomialOrder> rightBorderFaceValues(numberOfElementsY);
    borderValues2D<vector4D, polynomialOrder> topBorderFaceValues(numberOfElementsX);
    borderValues2D<vector4D, polynomialOrder> bottomBorderFaceValues(numberOfElementsX);
    
    dualFaceValues2D<vector4D, polynomialOrder> faceUValues(numberOfElementsX, numberOfElementsY);
    
    #pragma omp parallel for
    for (unsigned iElementX = 0; iElementX < numberOfElementsX; iElementX++)
    {
        #pragma omp parallel for
        for (unsigned jElementY = 0; jElementY < numberOfElementsY; jElementY++)
        {
            for (unsigned index = 0; index <= polynomialOrder; index++)
            {
                auto horizontal1DValues {pointUValues.values1DHorizontal(iElementX, jElementY, index)};
                
                vector4D& ULeft {faceUValues(iElementX, jElementY, Face::Left, index)};
                
                ULeft = lagrangePolynomial<polynomialOrder>::interpolateToLeftEdge(horizontal1DValues);
                
                vector4D& URight {faceUValues(iElementX, jElementY, Face::Right, index)};
                
                URight = lagrangePolynomial<polynomialOrder>::interpolateToRightEdge(horizontal1DValues);
                
                auto vertical1DValues {pointUValues.values1DVertical(iElementX, jElementY, index)};
                
                vector4D& UTop {faceUValues(iElementX, jElementY, Face::Top, index)};
                
                UTop = lagrangePolynomial<polynomialOrder>::interpolateToRightEdge(vertical1DValues);
                
                vector4D& UBottom {faceUValues(iElementX, jElementY, Face::Bottom, index)};
                
                UBottom = lagrangePolynomial<polynomialOrder>::interpolateToLeftEdge(vertical1DValues);
            }
        }
    }
    
    applyBoundaryConditions(leftBorderFaceValues, rightBorderFaceValues, topBorderFaceValues, bottomBorderFaceValues, faceUValues, mesh);
    
    constexpr auto points {getPoints<polynomialOrder>()};
    
    // common U physical values
    // TODO std::optional
    dualFaceValues2D<vector4D, polynomialOrder> UInterface(numberOfElementsX, numberOfElementsY);

    // TODO rework
    // for each element
    //     for each side (L, R, T, B)
    //         for each index
    if (viscous)
    {
        // inner faces - horizontal
        #pragma omp parallel for
        for (unsigned iElementX = 0; iElementX < numberOfElementsX; iElementX++)
        {
            for (unsigned jElementY = 0; jElementY < numberOfElementsY - 1; jElementY++)
            {
                for (unsigned index = 0; index <= polynomialOrder; index++)
                {
                    vector4D const& UBottom {faceUValues(iElementX, jElementY, Face::Top, index)};
                    vector4D const& UTop {faceUValues(iElementX, jElementY + 1, Face::Bottom, index)};
                    
                    auto const bottomJacobian {mesh.getJacobian(iElementX, jElementY, points[index], 1.)};
                    auto const topJacobian {mesh.getJacobian(iElementX, jElementY + 1, points[index], -1.)};
                    
                    auto const UBottomPhysical {transformValue<true>(UBottom, bottomJacobian)};
                    auto const UTopPhysical {transformValue<true>(UTop, topJacobian)};
                    
                    auto const UInterfacePhysical {(UTopPhysical + UBottomPhysical) / 2.};
                    
                    UInterface(iElementX, jElementY, Face::Top, index) = transformValue<false>(UInterfacePhysical, bottomJacobian);
                    UInterface(iElementX, jElementY + 1, Face::Bottom, index) = transformValue<false>(UInterfacePhysical, topJacobian);
                }
            }
        }
        
        // inner faces - vertical
        #pragma omp parallel for
        for (unsigned iElementX = 0; iElementX < numberOfElementsX - 1; iElementX++)
        {
            for (unsigned jElementY = 0; jElementY < numberOfElementsY; jElementY++)
            {
                for (unsigned index = 0; index <= polynomialOrder; index++)
                {
                    vector4D const& URight {faceUValues(iElementX + 1, jElementY, Face::Left, index)};
                    vector4D const& ULeft {faceUValues(iElementX, jElementY, Face::Right, index)};
                    
                    auto const rightJacobian {mesh.getJacobian(iElementX + 1, jElementY, -1., points[index])};
                    auto const leftJacobian {mesh.getJacobian(iElementX, jElementY, 1., points[index])};
                    
                    auto const URightPhysical {transformValue<true>(URight, rightJacobian)};
                    auto const ULeftPhysical {transformValue<true>(ULeft, leftJacobian)};
                    
                    auto const UInterfacePhysical {(ULeftPhysical + URightPhysical) / 2.};
                    
                    UInterface(iElementX, jElementY, Face::Right, index) = transformValue<false>(UInterfacePhysical, leftJacobian);
                    UInterface(iElementX + 1, jElementY, Face::Left, index) = transformValue<false>(UInterfacePhysical, rightJacobian);
                }
            }
        }
        
        for (unsigned iElementX = 0; iElementX < numberOfElementsX; iElementX++)
        {
            for (unsigned index = 0; index <= polynomialOrder; index++)
            {
                // top border
                {
                    auto const jacobian {mesh.getJacobian(iElementX, numberOfElementsY - 1, points[index], 1.)};
                    
                    vector4D const& UTopPhysical {topBorderFaceValues(iElementX, index)};
                    vector4D const UBottomPhysical {transformValue<true>(faceUValues(iElementX, numberOfElementsY - 1, Face::Top, index), jacobian)};
                    
                    auto const UInterfacePhysical {(UTopPhysical + UBottomPhysical) / 2.};
                    
                    UInterface(iElementX, numberOfElementsY - 1, Face::Top, index) = transformValue<false>(UInterfacePhysical, jacobian);
                }
                
                // bottom border
                {
                    auto const jacobian {mesh.getJacobian(iElementX, 0, points[index], -1.)};
                    
                    vector4D const UTopPhysical {transformValue<true>(faceUValues(iElementX, 0, Face::Bottom, index), jacobian)};
                    vector4D const& UBottomPhysical {bottomBorderFaceValues(iElementX, index)};
                    
                    auto const UInterfacePhysical {(UTopPhysical + UBottomPhysical) / 2.};
                    
                    UInterface(iElementX, 0, Face::Bottom, index) = transformValue<false>(UInterfacePhysical, jacobian);
                }
            }
        }
        
        for (unsigned jElementY = 0; jElementY < numberOfElementsY; jElementY++)
        {
            for (unsigned index = 0; index <= polynomialOrder; index++)
            {
                // left border
                {
                    auto const jacobian {mesh.getJacobian(0, jElementY, -1., points[index])};
                    
                    vector4D const& ULeftPhysical {leftBorderFaceValues(jElementY, index)};
                    vector4D const URightPhysical {transformValue<true>(faceUValues(0, jElementY, Face::Left, index), jacobian)};
                    
                    auto const UInterfacePhysical {(ULeftPhysical + URightPhysical) / 2.};
                    
                    UInterface(0, jElementY, Face::Left, index) = transformValue<false>(UInterfacePhysical, jacobian);
                }
                
                // right border
                {
                    auto const jacobian {mesh.getJacobian(numberOfElementsX - 1, jElementY, 1., points[index])};
                    
                    vector4D const ULeftPhysical {transformValue<true>(faceUValues(numberOfElementsX - 1, jElementY, Face::Right, index), jacobian)};
                    vector4D const& URightPhysical {rightBorderFaceValues(jElementY, index)};
                    
                    auto const UInterfacePhysical {(ULeftPhysical + URightPhysical) / 2.};
                    
                    UInterface(numberOfElementsX - 1, jElementY, Face::Right, index) = transformValue<false>(UInterfacePhysical, jacobian);
                }
            }
        }
    }
    
    #pragma omp parallel for
    for (unsigned iElementX = 0; iElementX < numberOfElementsX; iElementX++)
    {
        #pragma omp parallel for
        for (unsigned iElementY = 0; iElementY < numberOfElementsY; iElementY++)
        {
            for (unsigned jPointX = 0; jPointX < pointUValues.getNumberOfPointsX(iElementX, iElementY); jPointX++)
            {
                for (unsigned jPointY = 0; jPointY < pointUValues.getNumberOfPointsY(iElementX, iElementY); jPointY++)
                {
                    pointFluxFValues(iElementX, iElementY, jPointX, jPointY) =
                        computeFluxF
                        (
                            pointUValues(iElementX, iElementY, jPointX, jPointY),
                            mesh, points[jPointX], points[jPointY], iElementX, iElementY
                        );
                    
                    pointFluxGValues(iElementX, iElementY, jPointX, jPointY) =
                        computeFluxG
                        (
                            pointUValues(iElementX, iElementY, jPointX, jPointY),
                            mesh, points[jPointX], points[jPointY], iElementX, iElementY
                        );
                }
            }
        }
    }
    
    std::optional<pointValues2D<staticMatrix<4, 2>, polynomialOrder>> pointQValues;
    
    if (viscous)
    {
        pointQValues.emplace(numberOfElementsX, numberOfElementsY);

        #pragma omp parallel for
        for (unsigned iElementX = 0; iElementX < numberOfElementsX; iElementX++)
        {
            #pragma omp parallel for
            for (unsigned iElementY = 0; iElementY < numberOfElementsY; iElementY++)
            {
                for (unsigned jPointY = 0; jPointY < pointUValues.getNumberOfPointsY(iElementX, iElementY); jPointY++)
                {
                    auto const& leftFaceU {faceUValues(iElementX, iElementY, Face::Left, jPointY)};
                    auto const& rightFaceU {faceUValues(iElementX, iElementY, Face::Right, jPointY)};
                    
                    auto const& leftUInterface {UInterface(iElementX, iElementY, Face::Left, jPointY)};
                    auto const& rightUInterface {UInterface(iElementX, iElementY, Face::Right, jPointY)};
                    
                    for (unsigned jPointX = 0; jPointX < pointUValues.getNumberOfPointsX(iElementX, iElementY); jPointX++)
                    {
                        auto const UDerivativeXi
                        {
                            discontinuousFluxDerivativeAtPoint<polynomialOrder>(pointUValues.values1DHorizontal(iElementX, iElementY, jPointY), jPointX) +
                                (leftUInterface - leftFaceU) * correctionFunction<polynomialOrder>::getLeftDerivativeAtPoint(jPointX) +
                                (rightUInterface - rightFaceU) * correctionFunction<polynomialOrder>::getRightDerivativeAtPoint(jPointX)
                        };
                        
                        pointQValues.value()(iElementX, iElementY, jPointX, jPointY).assignRow(UDerivativeXi, 1);
                    }
                }
                
                for (unsigned jPointX = 0; jPointX < pointUValues.getNumberOfPointsX(iElementX, iElementY); jPointX++)
                {
                    auto const& topFaceU {faceUValues(iElementX, iElementY, Face::Top, jPointX)};
                    auto const& bottomFaceU {faceUValues(iElementX, iElementY, Face::Bottom, jPointX)};
                    
                    auto const& topUInterface {UInterface(iElementX, iElementY, Face::Top, jPointX)};
                    auto const& bottomUInterface {UInterface(iElementX, iElementY, Face::Bottom, jPointX)};
                    
                    for (unsigned jPointY = 0; jPointY < pointUValues.getNumberOfPointsY(iElementX, iElementY); jPointY++)
                    {
                        auto const UDerivativeEta
                        {
                            discontinuousFluxDerivativeAtPoint<polynomialOrder>(pointUValues.values1DVertical(iElementX, iElementY, jPointX), jPointY) +
                                (topUInterface - topFaceU) * correctionFunction<polynomialOrder>::getRightDerivativeAtPoint(jPointY) +
                                (bottomUInterface - bottomFaceU) * correctionFunction<polynomialOrder>::getLeftDerivativeAtPoint(jPointY)
                        };
                        
                        pointQValues.value()(iElementX, iElementY, jPointX, jPointY).assignRow(UDerivativeEta, 2);
                    }
                }
            }
        }
    }
    
    if (viscous)
    {
        #pragma omp parallel for
        for (unsigned iElementX = 0; iElementX < numberOfElementsX; iElementX++)
        {
            #pragma omp parallel for
            for (unsigned iElementY = 0; iElementY < numberOfElementsY; iElementY++)
            {
                for (unsigned jPointX = 0; jPointX < pointUValues.getNumberOfPointsX(iElementX, iElementY); jPointX++)
                {
                    for (unsigned jPointY = 0; jPointY < pointUValues.getNumberOfPointsY(iElementX, iElementY); jPointY++)
                    {
                        auto const& Q {pointQValues.value()(iElementX, iElementY, jPointX, jPointY)};
                        
                        auto inverseJacobianMatrix {mesh.getInverseJacobianMatrix(iElementX, iElementY, points[jPointX], points[jPointY])};
                        
                        auto const jacobian {mesh.getJacobian(iElementX, iElementY, points[jPointX], points[jPointY])};
                        
                        auto const QPhysical {transpose(inverseJacobianMatrix) * Q / jacobian};
                        
                        auto const DUDX {QPhysical.row(1)};
                        auto const DUDY {QPhysical.row(2)};
                        
                        pointFluxFValues(iElementX, iElementY, jPointX, jPointY) -=
                            computeFluxFv
                            (
                                pointUValues(iElementX, iElementY, jPointX, jPointY), DUDX, DUDY,
                                mesh, points[jPointX], points[jPointY], iElementX, iElementY
                            );
                        
                        pointFluxGValues(iElementX, iElementY, jPointX, jPointY) -=
                            computeFluxGv
                            (
                                pointUValues(iElementX, iElementY, jPointX, jPointY), DUDX, DUDY,
                                mesh, points[jPointX], points[jPointY], iElementX, iElementY
                            );
                    }
                }
            }
        }
    }
    
    // physical
    faceValues2D<vector4D, polynomialOrder> numericalFluxes(numberOfElementsX, numberOfElementsY);
    
    // inner faces - horizontal
    #pragma omp parallel for
    for (unsigned iElementX = 0; iElementX < numberOfElementsX; iElementX++)
    {
        #pragma omp parallel for
        for (unsigned jElementY = 0; jElementY < numberOfElementsY - 1; jElementY++)
        {
            for (unsigned index = 0; index <= polynomialOrder; index++)
            {
                auto const ULeft {faceUValues(iElementX, jElementY, Face::Top, index)};
                auto const URight {faceUValues(iElementX, jElementY + 1, Face::Bottom, index)};
                
                auto const leftJacobian {mesh.getJacobian(iElementX, jElementY, points[index], 1.)};
                auto const rightJacobian {mesh.getJacobian(iElementX, jElementY + 1, points[index], -1.)};
                
                auto const ULeftPhysical {transformValue<true>(ULeft, leftJacobian)};
                auto const URightPhysical {transformValue<true>(URight, rightJacobian)};
                
                auto const topNormalVectorPhysical {mesh.getNormalVector(iElementX, jElementY, Face::Top)};
                
                auto& numericalFlux {numericalFluxes(iElementX, jElementY, Face::Top, index)};
                
                numericalFlux = computeHLLCFlux(ULeftPhysical, URightPhysical, topNormalVectorPhysical);
                
                if (viscous)
                {
                    auto const partiallyCorrectedQLeftXi
                    {
                        discontinuousFluxDerivativeAtPoint<polynomialOrder>(faceUValues.values1D(iElementX, jElementY, Face::Top), index)
                    };
                    auto const partiallyCorrectedQLeftEta
                    {
                        discontinuousFluxDerivativeAtRightEdge<polynomialOrder>(pointUValues.values1DVertical(iElementX, jElementY, index)) +
                            (UInterface(iElementX, jElementY, Face::Top, index) - ULeft) * correctionFunction<polynomialOrder>::getRightDerivativeAtRightEdge()
                    };
                    auto const partiallyCorrectedQRightXi
                    {
                        discontinuousFluxDerivativeAtPoint<polynomialOrder>(faceUValues.values1D(iElementX, jElementY + 1, Face::Bottom), index)
                    };
                    auto const partiallyCorrectedQRightEta
                    {
                        discontinuousFluxDerivativeAtLeftEdge<polynomialOrder>(pointUValues.values1DVertical(iElementX, jElementY + 1, index)) +
                            (UInterface(iElementX, jElementY + 1, Face::Bottom, index) - URight) * correctionFunction<polynomialOrder>::getLeftDerivativeAtLeftEdge()
                    };
                    
                    staticMatrix<4, 2> partiallyCorrectedQLeft {};
                    partiallyCorrectedQLeft.assignRow(partiallyCorrectedQLeftXi, 1);
                    partiallyCorrectedQLeft.assignRow(partiallyCorrectedQLeftEta, 2);
                    
                    staticMatrix<4, 2> partiallyCorrectedQRight {};
                    partiallyCorrectedQRight.assignRow(partiallyCorrectedQRightXi, 1);
                    partiallyCorrectedQRight.assignRow(partiallyCorrectedQRightEta, 2);
                    
                    auto const leftInverseJacobianMatrix {mesh.getInverseJacobianMatrix(iElementX, jElementY, points[index], 1.)};
                    auto const rightInverseJacobianMatrix {mesh.getInverseJacobianMatrix(iElementX, jElementY + 1, points[index], -1.)};
                    
                    auto const partiallyCorrectedQLeftPhysical {transpose(leftInverseJacobianMatrix) * partiallyCorrectedQLeft / leftJacobian};
                    auto const partiallyCorrectedQRightPhysical {transpose(rightInverseJacobianMatrix) * partiallyCorrectedQRight / rightJacobian};
                    
                    auto const partiallyCorrectedQLeftX {partiallyCorrectedQLeftPhysical.row(1)};
                    auto const partiallyCorrectedQLeftY {partiallyCorrectedQLeftPhysical.row(2)};
                    auto const partiallyCorrectedQRightX {partiallyCorrectedQRightPhysical.row(1)};
                    auto const partiallyCorrectedQRightY {partiallyCorrectedQRightPhysical.row(2)};
                    
                    numericalFlux -= viscousFlux(ULeftPhysical, URightPhysical, partiallyCorrectedQLeftX, partiallyCorrectedQLeftY,
                                                 partiallyCorrectedQRightX, partiallyCorrectedQRightY, topNormalVectorPhysical);
                }
            }
        }
    }
    
    // inner faces - vertical
    #pragma omp parallel for
    for (unsigned iElementX = 0; iElementX < numberOfElementsX - 1; iElementX++)
    {
        #pragma omp parallel for
        for (unsigned jElementY = 0; jElementY < numberOfElementsY; jElementY++)
        {
            for (unsigned index = 0; index <= polynomialOrder; index++)
            {
                auto const ULeft {faceUValues(iElementX, jElementY, Face::Right, index)};
                auto const URight {faceUValues(iElementX + 1, jElementY, Face::Left, index)};
                
                auto const leftJacobian {mesh.getJacobian(iElementX, jElementY, 1., points[index])};
                auto const rightJacobian {mesh.getJacobian(iElementX + 1, jElementY, -1., points[index])};
                
                auto const ULeftPhysical {transformValue<true>(ULeft, leftJacobian)};
                auto const URightPhysical {transformValue<true>(URight, rightJacobian)};
                
                auto const rightNormalVectorPhysical {mesh.getNormalVector(iElementX, jElementY, Face::Right)};
                
                auto& numericalFlux {numericalFluxes(iElementX, jElementY, Face::Right, index)};
                
                numericalFlux = computeHLLCFlux(ULeftPhysical, URightPhysical, rightNormalVectorPhysical);
                
                if (viscous)
                {
                    auto const partiallyCorrectedQLeftXi
                    {
                        discontinuousFluxDerivativeAtRightEdge<polynomialOrder>(pointUValues.values1DHorizontal(iElementX, jElementY, index)) +
                            (UInterface(iElementX, jElementY, Face::Right, index) - ULeft) * correctionFunction<polynomialOrder>::getRightDerivativeAtRightEdge()
                    };
                    auto const partiallyCorrectedQLeftEta
                    {
                        discontinuousFluxDerivativeAtPoint<polynomialOrder>(faceUValues.values1D(iElementX, jElementY, Face::Right), index)
                    };
                    auto const partiallyCorrectedQRightXi
                    {
                        discontinuousFluxDerivativeAtRightEdge<polynomialOrder>(pointUValues.values1DHorizontal(iElementX + 1, jElementY, index)) +
                            (UInterface(iElementX + 1, jElementY, Face::Left, index) - URight) * correctionFunction<polynomialOrder>::getLeftDerivativeAtLeftEdge()
                    };
                    auto const partiallyCorrectedQRightEta
                    {
                        discontinuousFluxDerivativeAtPoint<polynomialOrder>(faceUValues.values1D(iElementX + 1, jElementY, Face::Left), index)
                    };
                    
                    staticMatrix<4, 2> partiallyCorrectedQLeft {};
                    partiallyCorrectedQLeft.assignRow(partiallyCorrectedQLeftXi, 1);
                    partiallyCorrectedQLeft.assignRow(partiallyCorrectedQLeftEta, 2);
                    
                    staticMatrix<4, 2> partiallyCorrectedQRight {};
                    partiallyCorrectedQRight.assignRow(partiallyCorrectedQRightXi, 1);
                    partiallyCorrectedQRight.assignRow(partiallyCorrectedQRightEta, 2);
                    
                    auto const leftInverseJacobianMatrix {mesh.getInverseJacobianMatrix(iElementX, jElementY, 1., points[index])};
                    auto const rightInverseJacobianMatrix {mesh.getInverseJacobianMatrix(iElementX + 1, jElementY, -1., points[index])};
                    
                    auto const partiallyCorrectedQLeftPhysical {transpose(leftInverseJacobianMatrix) * partiallyCorrectedQLeft / leftJacobian};
                    auto const partiallyCorrectedQRightPhysical {transpose(rightInverseJacobianMatrix) * partiallyCorrectedQRight / rightJacobian};
                    
                    auto const partiallyCorrectedQLeftX {partiallyCorrectedQLeftPhysical.row(1)};
                    auto const partiallyCorrectedQLeftY {partiallyCorrectedQLeftPhysical.row(2)};
                    auto const partiallyCorrectedQRightX {partiallyCorrectedQRightPhysical.row(1)};
                    auto const partiallyCorrectedQRightY {partiallyCorrectedQRightPhysical.row(2)};
                    
                    numericalFlux -= viscousFlux(ULeftPhysical, URightPhysical, partiallyCorrectedQLeftX, partiallyCorrectedQLeftY,
                                                 partiallyCorrectedQRightX, partiallyCorrectedQRightY, rightNormalVectorPhysical);
                }
            }
        }
    }
    
    // top border
    // #pragma omp parallel for
    for (unsigned iElementX = 0; iElementX < numberOfElementsX; iElementX++)
    {
        for (unsigned index = 0; index <= polynomialOrder; index++)
        {
            auto const jacobian {mesh.getJacobian(iElementX, numberOfElementsY - 1, points[index], 1.)};
            
            auto const ULeft {faceUValues(iElementX, numberOfElementsY - 1, Face::Top, index)};
            
            auto const ULeftPhysical {transformValue<true>(ULeft, jacobian)};
            auto const URightPhysical {topBorderFaceValues(iElementX, index)};
            
            auto const topNormalVectorPhysical {mesh.getNormalVector(iElementX, numberOfElementsY - 1, Face::Top)};
            
            auto& numericalFlux {numericalFluxes(iElementX, numberOfElementsY - 1, Face::Top, index)};
            
            numericalFlux = computeHLLCFlux(ULeftPhysical, URightPhysical, topNormalVectorPhysical);
            
            if (viscous)
            {
                auto const partiallyCorrectedQLeftXi
                {
                    discontinuousFluxDerivativeAtPoint<polynomialOrder>(faceUValues.values1D(iElementX, numberOfElementsY - 1, Face::Top), index)
                };
                auto const partiallyCorrectedQLeftEta
                {
                    discontinuousFluxDerivativeAtRightEdge<polynomialOrder>(pointUValues.values1DVertical(iElementX, numberOfElementsY - 1, index)) +
                        (UInterface(iElementX, numberOfElementsY - 1, Face::Top, index) - ULeft) * correctionFunction<polynomialOrder>::getRightDerivativeAtRightEdge()
                };
                auto const partiallyCorrectedQRightXi {partiallyCorrectedQLeftXi};
                auto const partiallyCorrectedQRightEta {partiallyCorrectedQLeftEta};
                
                staticMatrix<4, 2> partiallyCorrectedQLeft {};
                partiallyCorrectedQLeft.assignRow(partiallyCorrectedQLeftXi, 1);
                partiallyCorrectedQLeft.assignRow(partiallyCorrectedQLeftEta, 2);
                
                staticMatrix<4, 2> partiallyCorrectedQRight {};
                partiallyCorrectedQRight.assignRow(partiallyCorrectedQRightXi, 1);
                partiallyCorrectedQRight.assignRow(partiallyCorrectedQRightEta, 2);
                
                auto const inverseJacobianMatrix {mesh.getInverseJacobianMatrix(iElementX, numberOfElementsY - 1, points[index], 1.)};
                
                auto const partiallyCorrectedQLeftPhysical {transpose(inverseJacobianMatrix) * partiallyCorrectedQLeft / jacobian};
                auto const partiallyCorrectedQRightPhysical {transpose(inverseJacobianMatrix) * partiallyCorrectedQRight / jacobian};
                
                auto const partiallyCorrectedQLeftX {partiallyCorrectedQLeftPhysical.row(1)};
                auto const partiallyCorrectedQLeftY {partiallyCorrectedQLeftPhysical.row(2)};
                auto const partiallyCorrectedQRightX {partiallyCorrectedQRightPhysical.row(1)};
                auto const partiallyCorrectedQRightY {partiallyCorrectedQRightPhysical.row(2)};
                
                numericalFlux -= viscousFlux(ULeftPhysical, URightPhysical, partiallyCorrectedQLeftX, partiallyCorrectedQLeftY,
                                             partiallyCorrectedQRightX, partiallyCorrectedQRightY, topNormalVectorPhysical);
            }
        }
    }
    
    // bottom border
    // #pragma omp parallel for
    for (unsigned iElementX = 0; iElementX < numberOfElementsX; iElementX++)
    {
        for (unsigned index = 0; index <= polynomialOrder; index++)
        {
            auto const jacobian {mesh.getJacobian(iElementX, 0, points[index], -1.)};
            
            auto const URight {faceUValues(iElementX, 0, Face::Bottom, index)};
            
            auto const ULeftPhysical {bottomBorderFaceValues(iElementX, index)};
            auto const URightPhysical {transformValue<true>(URight, jacobian)};
            
            auto const bottomNormalVectorPhysical {mesh.getNormalVector(iElementX, 0, Face::Bottom)};
            
            auto& numericalFlux {numericalFluxes(iElementX, 0, Face::Bottom, index)};
            
            numericalFlux = computeHLLCFlux(ULeftPhysical, URightPhysical, -1. * bottomNormalVectorPhysical);

            if (viscous)
            {
                auto const partiallyCorrectedQRightXi
                {
                    discontinuousFluxDerivativeAtPoint<polynomialOrder>(faceUValues.values1D(iElementX, 0, Face::Bottom), index)
                };
                auto const partiallyCorrectedQRightEta
                {
                    discontinuousFluxDerivativeAtLeftEdge<polynomialOrder>(pointUValues.values1DVertical(iElementX, 0, index)) +
                        (UInterface(iElementX, 0, Face::Bottom, index) - URight) * correctionFunction<polynomialOrder>::getLeftDerivativeAtLeftEdge()
                };
                
                auto const partiallyCorrectedQLeftXi {partiallyCorrectedQRightXi};
                auto const partiallyCorrectedQLeftEta {partiallyCorrectedQRightEta};
                
                staticMatrix<4, 2> partiallyCorrectedQLeft {};
                partiallyCorrectedQLeft.assignRow(partiallyCorrectedQLeftXi, 1);
                partiallyCorrectedQLeft.assignRow(partiallyCorrectedQLeftEta, 2);
                
                staticMatrix<4, 2> partiallyCorrectedQRight {};
                partiallyCorrectedQRight.assignRow(partiallyCorrectedQRightXi, 1);
                partiallyCorrectedQRight.assignRow(partiallyCorrectedQRightEta, 2);
                
                auto const inverseJacobianMatrix {mesh.getInverseJacobianMatrix(iElementX, 0, points[index], -1.)};
                
                auto const partiallyCorrectedQLeftPhysical {transpose(inverseJacobianMatrix) * partiallyCorrectedQLeft / jacobian};
                auto const partiallyCorrectedQRightPhysical {transpose(inverseJacobianMatrix) * partiallyCorrectedQRight / jacobian};
                
                auto const partiallyCorrectedQLeftX {partiallyCorrectedQLeftPhysical.row(1)};
                auto const partiallyCorrectedQLeftY {partiallyCorrectedQLeftPhysical.row(2)};
                auto const partiallyCorrectedQRightX {partiallyCorrectedQRightPhysical.row(1)};
                auto const partiallyCorrectedQRightY {partiallyCorrectedQRightPhysical.row(2)};
                
                numericalFlux -= viscousFlux(ULeftPhysical, URightPhysical, partiallyCorrectedQLeftX, partiallyCorrectedQLeftY,
                                             partiallyCorrectedQRightX, partiallyCorrectedQRightY, -1 * bottomNormalVectorPhysical, true);
            }
        }
    }
    
    // left border
    // #pragma omp parallel for
    for (unsigned jElementY = 0; jElementY < numberOfElementsY; jElementY++)
    {
        for (unsigned index = 0; index <= polynomialOrder; index++)
        {
            auto const jacobian {mesh.getJacobian(0, jElementY, -1., points[index])};
            
            auto const URight {faceUValues(0, jElementY, Face::Left, index)};
            
            auto const ULeftPhysical {leftBorderFaceValues(jElementY, index)};
            auto const URightPhysical {transformValue<true>(URight, jacobian)};
            
            auto const leftNormalVectorPhysical {mesh.getNormalVector(0, jElementY, Face::Left)};
            
            auto& numericalFlux {numericalFluxes(0, jElementY, Face::Left, index)};
            
            numericalFlux = computeHLLCFlux(ULeftPhysical, URightPhysical, -1. * leftNormalVectorPhysical);
            
            if (viscous)
            {
                auto const partiallyCorrectedQRightXi
                {
                    discontinuousFluxDerivativeAtLeftEdge<polynomialOrder>(pointUValues.values1DHorizontal(0, jElementY, index)) +
                        (UInterface(0, jElementY, Face::Left, index) - URight) * correctionFunction<polynomialOrder>::getLeftDerivativeAtLeftEdge()
                };
                auto const partiallyCorrectedQRightEta
                {
                    discontinuousFluxDerivativeAtPoint<polynomialOrder>(faceUValues.values1D(0, jElementY, Face::Left), index)
                };
                
                auto const partiallyCorrectedQLeftXi {partiallyCorrectedQRightXi};
                auto const partiallyCorrectedQLeftEta {partiallyCorrectedQRightEta};
                
                staticMatrix<4, 2> partiallyCorrectedQLeft {};
                partiallyCorrectedQLeft.assignRow(partiallyCorrectedQLeftXi, 1);
                partiallyCorrectedQLeft.assignRow(partiallyCorrectedQLeftEta, 2);
                
                staticMatrix<4, 2> partiallyCorrectedQRight {};
                partiallyCorrectedQRight.assignRow(partiallyCorrectedQRightXi, 1);
                partiallyCorrectedQRight.assignRow(partiallyCorrectedQRightEta, 2);
                
                auto const inverseJacobianMatrix {mesh.getInverseJacobianMatrix(0, jElementY, -1., points[index])};
                
                auto const partiallyCorrectedQLeftPhysical {transpose(inverseJacobianMatrix) * partiallyCorrectedQLeft / jacobian};
                auto const partiallyCorrectedQRightPhysical {transpose(inverseJacobianMatrix) * partiallyCorrectedQRight / jacobian};
                
                auto const partiallyCorrectedQLeftX {partiallyCorrectedQLeftPhysical.row(1)};
                auto const partiallyCorrectedQLeftY {partiallyCorrectedQLeftPhysical.row(2)};
                auto const partiallyCorrectedQRightX {partiallyCorrectedQRightPhysical.row(1)};
                auto const partiallyCorrectedQRightY {partiallyCorrectedQRightPhysical.row(2)};
                
                numericalFlux -= viscousFlux(ULeftPhysical, URightPhysical, partiallyCorrectedQLeftX, partiallyCorrectedQLeftY,
                                             partiallyCorrectedQRightX, partiallyCorrectedQRightY, -1 * leftNormalVectorPhysical);
            }
        }
    }
    
    // right border
    // #pragma omp parallel for
    for (unsigned jElementY = 0; jElementY < numberOfElementsY; jElementY++)
    {
        for (unsigned index = 0; index <= polynomialOrder; index++)
        {
            auto const jacobian {mesh.getJacobian(numberOfElementsX - 1, jElementY, 1., points[index])};
            
            auto const ULeft {faceUValues(numberOfElementsX - 1, jElementY, Face::Right, index)};
            
            auto const ULeftPhysical {transformValue<true>(ULeft, jacobian)};
            auto const URightPhysical {rightBorderFaceValues(jElementY, index)};
            
            auto const rightNormalVectorPhysical {mesh.getNormalVector(numberOfElementsX - 1, jElementY, Face::Right)};
            
            auto& numericalFlux {numericalFluxes(numberOfElementsX - 1, jElementY, Face::Right, index)};
            
            numericalFlux = computeHLLCFlux(ULeftPhysical, URightPhysical, rightNormalVectorPhysical);
            
            if (viscous)
            {
                auto const partiallyCorrectedQLeftXi
                {
                    discontinuousFluxDerivativeAtRightEdge<polynomialOrder>(pointUValues.values1DHorizontal(numberOfElementsX - 1, jElementY, index)) +
                        (UInterface(numberOfElementsX - 1, jElementY, Face::Right, index) - ULeft) * correctionFunction<polynomialOrder>::getRightDerivativeAtRightEdge()
                };
                auto const partiallyCorrectedQLeftEta
                {
                    discontinuousFluxDerivativeAtPoint<polynomialOrder>(faceUValues.values1D(numberOfElementsX - 1, jElementY, Face::Right), index)
                };
                auto const partiallyCorrectedQRightXi {partiallyCorrectedQLeftXi};
                auto const partiallyCorrectedQRightEta {partiallyCorrectedQLeftEta};
                
                staticMatrix<4, 2> partiallyCorrectedQLeft {};
                partiallyCorrectedQLeft.assignRow(partiallyCorrectedQLeftXi, 1);
                partiallyCorrectedQLeft.assignRow(partiallyCorrectedQLeftEta, 2);
                
                staticMatrix<4, 2> partiallyCorrectedQRight {};
                partiallyCorrectedQRight.assignRow(partiallyCorrectedQRightXi, 1);
                partiallyCorrectedQRight.assignRow(partiallyCorrectedQRightEta, 2);
                
                auto const inverseJacobianMatrix {mesh.getInverseJacobianMatrix(numberOfElementsX - 1, jElementY, 1., points[index])};
                
                auto const partiallyCorrectedQLeftPhysical {transpose(inverseJacobianMatrix) * partiallyCorrectedQLeft / jacobian};
                auto const partiallyCorrectedQRightPhysical {transpose(inverseJacobianMatrix) * partiallyCorrectedQRight / jacobian};
                
                auto const partiallyCorrectedQLeftX {partiallyCorrectedQLeftPhysical.row(1)};
                auto const partiallyCorrectedQLeftY {partiallyCorrectedQLeftPhysical.row(2)};
                auto const partiallyCorrectedQRightX {partiallyCorrectedQRightPhysical.row(1)};
                auto const partiallyCorrectedQRightY {partiallyCorrectedQRightPhysical.row(2)};
                
                numericalFlux -= viscousFlux(ULeftPhysical, URightPhysical, partiallyCorrectedQLeftX, partiallyCorrectedQLeftY,
                                             partiallyCorrectedQRightX, partiallyCorrectedQRightY, rightNormalVectorPhysical);
            }
        }
    }
    
    // TODO alokace
    pointValues2D<vector4D, polynomialOrder> newPointUValues(numberOfElementsX, numberOfElementsY);
    
    #pragma omp parallel for
    for (unsigned iElementX = 0; iElementX < numberOfElementsX; iElementX++)
    {
        #pragma omp parallel for
        for (unsigned iElementY = 0; iElementY < numberOfElementsY; iElementY++)
        {
            for (unsigned jPointY = 0; jPointY < pointUValues.getNumberOfPointsY(iElementX, iElementY); jPointY++)
            {
                auto const leftFaceFlux {lagrangePolynomial<polynomialOrder>::interpolateToLeftEdge(pointFluxFValues.values1DHorizontal(iElementX, iElementY, jPointY))};
                auto const rightFaceFlux {lagrangePolynomial<polynomialOrder>::interpolateToRightEdge(pointFluxFValues.values1DHorizontal(iElementX, iElementY, jPointY))};
                
                auto const leftNormalVectorPhysical {mesh.getNormalVector(iElementX, iElementY, Face::Left)};
                auto const rightNormalVectorPhysical {mesh.getNormalVector(iElementX, iElementY, Face::Right)};
                
                vector2D const leftNormalVectorTransformed
                {
                    mesh.getXDerivativeXi(iElementX, iElementY, -1., points[jPointY]) * leftNormalVectorPhysical.x() +
                    mesh.getYDerivativeXi(iElementX, iElementY, -1., points[jPointY]) * leftNormalVectorPhysical.y(),
                    mesh.getXDerivativeEta(iElementX, iElementY, -1., points[jPointY]) * leftNormalVectorPhysical.x() +
                    mesh.getYDerivativeEta(iElementX, iElementY, -1., points[jPointY]) * leftNormalVectorPhysical.y()
                };
                
                vector2D const rightNormalVectorTransformed
                {
                    mesh.getXDerivativeXi(iElementX, iElementY, 1., points[jPointY]) * rightNormalVectorPhysical.x() +
                    mesh.getYDerivativeXi(iElementX, iElementY, 1., points[jPointY]) * rightNormalVectorPhysical.y(),
                    mesh.getXDerivativeEta(iElementX, iElementY, 1., points[jPointY]) * rightNormalVectorPhysical.x() +
                    mesh.getYDerivativeEta(iElementX, iElementY, 1., points[jPointY]) * rightNormalVectorPhysical.y()
                };
                
                auto const leftNumericalFlux {numericalFluxes(iElementX, iElementY, Face::Left, jPointY) *
                        mesh.getJacobian(iElementX, iElementY, -1., points[jPointY]) / norm(leftNormalVectorTransformed)};
                auto const rightNumericalFlux {numericalFluxes(iElementX, iElementY, Face::Right, jPointY) *
                        mesh.getJacobian(iElementX, iElementY, 1., points[jPointY]) / norm(rightNormalVectorTransformed)};
                
                for (unsigned jPointX = 0; jPointX < pointUValues.getNumberOfPointsX(iElementX, iElementY); jPointX++)
                {
                    auto const fluxFDerivative
                    {
                        discontinuousFluxDerivativeAtPoint<polynomialOrder>(pointFluxFValues.values1DHorizontal(iElementX, iElementY, jPointY), jPointX) +
                            (leftNumericalFlux - leftFaceFlux) * correctionFunction<polynomialOrder>::getLeftDerivativeAtPoint(jPointX) +
                            (rightNumericalFlux - rightFaceFlux) * correctionFunction<polynomialOrder>::getRightDerivativeAtPoint(jPointX)
                    };
                    
                    newPointUValues(iElementX, iElementY, jPointX, jPointY) = fluxFDerivative;
                }
            }
            
            for (unsigned jPointX = 0; jPointX < pointUValues.getNumberOfPointsX(iElementX, iElementY); jPointX++)
            {
                auto const topFaceFlux {lagrangePolynomial<polynomialOrder>::interpolateToRightEdge(pointFluxGValues.values1DVertical(iElementX, iElementY, jPointX))};
                auto const bottomFaceFlux {lagrangePolynomial<polynomialOrder>::interpolateToLeftEdge(pointFluxGValues.values1DVertical(iElementX, iElementY, jPointX))};
                
                auto const topNormalVectorPhysical {mesh.getNormalVector(iElementX, iElementY, Face::Top)};
                auto const bottomNormalVectorPhysical {mesh.getNormalVector(iElementX, iElementY, Face::Bottom)};
                
                vector2D const topNormalVectorTransformed
                {
                    mesh.getXDerivativeXi(iElementX, iElementY, points[jPointX], 1.) * topNormalVectorPhysical.x() +
                    mesh.getYDerivativeXi(iElementX, iElementY, points[jPointX], 1.) * topNormalVectorPhysical.y(),
                    mesh.getXDerivativeEta(iElementX, iElementY, points[jPointX], 1.) * topNormalVectorPhysical.x() +
                    mesh.getYDerivativeEta(iElementX, iElementY, points[jPointX], 1.) * topNormalVectorPhysical.y()
                };
                
                vector2D const bottomNormalVectorTransformed
                {
                    mesh.getXDerivativeXi(iElementX, iElementY, points[jPointX], -1.) * bottomNormalVectorPhysical.x() +
                    mesh.getYDerivativeXi(iElementX, iElementY, points[jPointX], -1.) * bottomNormalVectorPhysical.y(),
                    mesh.getXDerivativeEta(iElementX, iElementY, points[jPointX], -1.) * bottomNormalVectorPhysical.x() +
                    mesh.getYDerivativeEta(iElementX, iElementY, points[jPointX], -1.) * bottomNormalVectorPhysical.y()
                };
                
                auto const topNumericalFlux {numericalFluxes(iElementX, iElementY, Face::Top, jPointX) *
                        mesh.getJacobian(iElementX, iElementY, points[jPointX], 1.) / norm(topNormalVectorTransformed)};
                auto const bottomNumericalFlux {numericalFluxes(iElementX, iElementY, Face::Bottom, jPointX) *
                        mesh.getJacobian(iElementX, iElementY, points[jPointX], -1.) / norm(bottomNormalVectorTransformed)};
                
                for (unsigned jPointY = 0; jPointY < pointUValues.getNumberOfPointsY(iElementX, iElementY); jPointY++)
                {
                    auto const fluxGDerivative
                    {
                        discontinuousFluxDerivativeAtPoint<polynomialOrder>(pointFluxGValues.values1DVertical(iElementX, iElementY, jPointX), jPointY) +
                            (topNumericalFlux - topFaceFlux) * correctionFunction<polynomialOrder>::getRightDerivativeAtPoint(jPointY) +
                            (bottomNumericalFlux - bottomFaceFlux) * correctionFunction<polynomialOrder>::getLeftDerivativeAtPoint(jPointY)
                    };
                     
                    newPointUValues(iElementX, iElementY, jPointX, jPointY) += fluxGDerivative;
                    
                    newPointUValues(iElementX, iElementY, jPointX, jPointY) *= timeSteps(iElementX, iElementY, jPointX, jPointY);
                }
            }
        }
    }
    
    return newPointUValues;
}

#endif
