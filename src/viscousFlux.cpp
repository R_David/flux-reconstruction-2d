#include "utility.h"
#include "utilityFunctions.h"
#include "viscousFlux.h"

#include <cmath>
#include <iostream>

vector4D viscousFlux (vector4D const& ULeft, vector4D const& URight,
                      vector4D const& QLeftX, vector4D const& QLeftY,
                      vector4D const& QRightX, vector4D const& QRightY,
                      vector2D normalVector, bool output)
{
    auto const FLeft {computeFluxFvPhysical(ULeft, QLeftX, QLeftY)};
    auto const FRight {computeFluxFvPhysical(URight, QRightX, QRightY)};
    
    auto const GLeft {computeFluxGvPhysical(ULeft, QLeftX, QLeftY)};
    auto const GRight {computeFluxGvPhysical(URight, QRightX, QRightY)};
    
    vector4D FnLeft {FLeft * normalVector.x() + GLeft * normalVector.y()};
    vector4D FnRight {FRight * normalVector.x() + GRight * normalVector.y()};
    
    return (FnLeft + FnRight) / 2.;
}
