#ifndef STATICMATRIX_H_INCLUDED
#define STATICMATRIX_H_INCLUDED

#include <array>

#include "myVector.h"

template <unsigned columns, unsigned rows>
class staticMatrix
{
public:
    
    staticMatrix () {};
    staticMatrix (staticMatrix<columns, rows> const& other) : data(other.data) {};
    
    ~staticMatrix () {};
    
    staticMatrix<columns, rows>& operator= (staticMatrix const& other);
    
    double& operator() (unsigned row, unsigned column);
    double const& operator() (unsigned row, unsigned column) const;
    
    void assignRow (int iRow, std::array<double, columns> values);
    void assignColumn (int iColumn, std::array<double, rows> values);
    
    template <unsigned otherColumns>
    staticMatrix<otherColumns, columns> operator* (staticMatrix<otherColumns, columns> const& otherMatrix);
    
    staticMatrix<columns, rows> operator* (double coefficient);
    staticMatrix<columns, rows>& operator*= (double coefficient);
    staticMatrix<columns, rows> operator/ (double coefficient);
    
    vector4D const row (unsigned iRow) const;
    void assignRow (vector4D const& row, unsigned iRow);
    
private:
    
    std::array<double, rows * columns> data {};
};

template <unsigned columns, unsigned rows>
staticMatrix<columns, rows> operator+ (staticMatrix<columns, rows> const& A, staticMatrix<columns, rows> const& B)
{
    staticMatrix<columns, rows> result;
    
    for (unsigned iRow {1}; iRow <= rows; iRow++)
    {
        for (unsigned iColumn {1}; iColumn <= columns; iColumn++)
        {
            result(iRow, iColumn) = A(iRow, iColumn) + B(iRow, iColumn);
        }
    }
    
    return result;
}

template <unsigned columns, unsigned rows>
staticMatrix<columns, rows> operator* (staticMatrix<columns, rows> const& matrix, double coefficient)
{
    staticMatrix<columns, rows> result;
    
    for (unsigned iRow {1}; iRow <= rows; iRow++)
    {
        for (unsigned iColumn {1}; iColumn <= columns; iColumn++)
        {
            result(iRow, iColumn) = matrix(iRow, iColumn) * coefficient;
        }
    }
    
    return result;
}

template <unsigned columns, unsigned rows>
staticMatrix<columns, rows> operator* (double coefficient, staticMatrix<columns, rows> const& matrix)
{
    staticMatrix<columns, rows> result;
    
    for (unsigned iRow {1}; iRow <= rows; iRow++)
    {
        for (unsigned iColumn {1}; iColumn <= columns; iColumn++)
        {
            result(iRow, iColumn) = matrix(iRow, iColumn) * coefficient;
        }
    }
    
    return result;
}

template <unsigned columns, unsigned rows>
staticMatrix<rows, columns> transpose (staticMatrix<columns, rows> matrix);

#endif
