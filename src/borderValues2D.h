#ifndef BORDER_VALUES_2D_H
#define BORDER_VALUES_2D_H

#include <vector>

template <typename Type, unsigned numericalOrder>
class borderValues2D
{
public:
    
    borderValues2D (unsigned numberOfElements)
    {
        values.resize(numberOfElements * (numericalOrder + 1));
    }
    
    Type& operator() (unsigned iElement, unsigned index)
    {
        return values[iElement * (numericalOrder + 1) + index];
    }

    Type const& operator() (unsigned iElement, unsigned index) const
    {
        return values[iElement * (numericalOrder + 1) + index];
    }
    
private:
    
    std::vector<Type> values;
};

#endif
