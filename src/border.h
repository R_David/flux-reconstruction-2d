#ifndef BORDER_H
#define BORDER_H

#include "parametricFunction.h"

class border
{
public:
    border (parametricFunction&& function_, parametricFunction&& inverseFunction_, parametricFunction&& derivative_,
            double u1_, double u2_, double v1_, double v2_) :
    function (std::move(function_)), inverseFunction (inverseFunction_), derivative (derivative_), u1 (u1_), u2 (u2_), v1 (v1_), v2 (v2_) {};
    
    border () {};
    
    point2D operator() (double u, double v) const
    {
        return function(u1 + u * (u2 - u1),  v1 + v * (v2 - v1));
    };
    
    point2D parametricCoordinate (double x, double y) const
    {
        auto nonScaledCoordinate {inverseFunction(x, y)};
        
        return {(u1 == u2) ? 0 : ((nonScaledCoordinate.x() - u1) / (u2 - u1)), (v1 == v2) ? 0 : ((nonScaledCoordinate.y() - v1) / (v2 - v1))};
    };
    
    point2D getDerivative (double u, double v) const
    {
        return derivative(u1 + u * (u2 - u1), v1 + v * (v2 - v1));
    };
    
private:
    
    parametricFunction function;
    parametricFunction inverseFunction;
    parametricFunction derivative;
    
    double u1, u2, v1, v2;
};

#endif
