#ifndef DUAL_FACE_VALUES_2D_H
#define DUAL_FACE_VALUES_2D_H

#include <span>
#include <vector>

#include "utility.h"

template <typename Type, unsigned numericalOrder>
class dualFaceValues2D
{

public:
    
    dualFaceValues2D (unsigned numberOfElementsX_, unsigned numberOfElementsY_) :
        numberOfElementsX (numberOfElementsX_),
        numberOfElementsY (numberOfElementsY_)
    {
        values.resize(4 * numberOfElementsX * numberOfElementsY * (numericalOrder + 1));
    };
    
    Type& operator() (unsigned iElementX, unsigned jElementY, Face facePosition, int index)
    {
        switch (facePosition)
        {
            case Face::Top:
                return values[(jElementY * numberOfElementsX + iElementX) * 4 * (numericalOrder + 1) + 2 * (numericalOrder + 1) + (numericalOrder - index)];
                // return values[(jElementY * numberOfElementsX + iElementX) * 4 * (numericalOrder + 1) + 2 * (numericalOrder + 1) + index];
            case Face::Bottom:
                return values[(jElementY * numberOfElementsX + iElementX) * 4 * (numericalOrder + 1) + index];
            case Face::Left:
                return values[(jElementY * numberOfElementsX + iElementX) * 4 * (numericalOrder + 1) + 3 * (numericalOrder + 1) + (numericalOrder - index)];
                // return values[(jElementY * numberOfElementsX + iElementX) * 4 * (numericalOrder + 1) + 3 * (numericalOrder + 1) + index];
            case Face::Right:
                return values[(jElementY * numberOfElementsX + iElementX) * 4 * (numericalOrder + 1) + (numericalOrder + 1) + index];
            default:
                return values.front();
        }
    };

    Type const& operator() (unsigned iElementX, unsigned jElementY, Face facePosition, int index) const
    {
        switch (facePosition)
        {
            case Face::Top:
                return values[(jElementY * numberOfElementsX + iElementX) * 4 * (numericalOrder + 1) + 2 * (numericalOrder + 1) + (numericalOrder - index)];
                // return values[(jElementY * numberOfElementsX + iElementX) * 4 * (numericalOrder + 1) + 2 * (numericalOrder + 1) + index];
            case Face::Bottom:
                return values[(jElementY * numberOfElementsX + iElementX) * 4 * (numericalOrder + 1) + index];
            case Face::Left:
                return values[(jElementY * numberOfElementsX + iElementX) * 4 * (numericalOrder + 1) + 3 * (numericalOrder + 1) + (numericalOrder - index)];
                // return values[(jElementY * numberOfElementsX + iElementX) * 4 * (numericalOrder + 1) + 3 * (numericalOrder + 1) + index];
            case Face::Right:
                return values[(jElementY * numberOfElementsX + iElementX) * 4 * (numericalOrder + 1) + (numericalOrder + 1) + index];
            default:
                return values.front();
        }
    };
    
    std::span<Type, numericalOrder + 1> values1D (unsigned iElementX, unsigned jElementY, Face facePosition)
    {
        switch (facePosition)
        {
            case Face::Top:
            {
                unsigned const startingIndex {(jElementY * numberOfElementsX + iElementX) * 4 * (numericalOrder + 1) + 2 * (numericalOrder + 1)};
                return std::span<Type, numericalOrder + 1> {&values[startingIndex], numericalOrder + 1};
            }   
            case Face::Bottom:
            {
                unsigned const startingIndex {(jElementY * numberOfElementsX + iElementX) * 4 * (numericalOrder + 1)};
                return std::span<Type, numericalOrder + 1> {&values[startingIndex], numericalOrder + 1};
            }   
            case Face::Left:
            {
                unsigned const startingIndex {(jElementY * numberOfElementsX + iElementX) * 4 * (numericalOrder + 1) + 3 * (numericalOrder + 1)};
                return std::span<Type, numericalOrder + 1> {&values[startingIndex], numericalOrder + 1};
            }
            case Face::Right:
            {
                unsigned const startingIndex {(jElementY * numberOfElementsX + iElementX) * 4 * (numericalOrder + 1) + (numericalOrder + 1)};
                return std::span<Type, numericalOrder + 1> {&values[startingIndex], numericalOrder + 1};
            }
            default:
                __builtin_unreachable();
        }
    };
    
    std::span<Type const, numericalOrder + 1> values1D (unsigned iElementX, unsigned jElementY, Face facePosition) const
    {
        switch (facePosition)
        {
            case Face::Top:
            {
                unsigned const startingIndex {(jElementY * numberOfElementsX + iElementX) * 4 * (numericalOrder + 1) + 2 * (numericalOrder + 1)};
                return std::span<Type const, numericalOrder + 1> {&values[startingIndex], numericalOrder + 1};
            }   
            case Face::Bottom:
            {
                unsigned const startingIndex {(jElementY * numberOfElementsX + iElementX) * 4 * (numericalOrder + 1)};
                return std::span<Type const, numericalOrder + 1> {&values[startingIndex], numericalOrder + 1};
            }   
            case Face::Left:
            {
                unsigned const startingIndex {(jElementY * numberOfElementsX + iElementX) * 4 * (numericalOrder + 1) + 3 * (numericalOrder + 1)};
                return std::span<Type const, numericalOrder + 1> {&values[startingIndex], numericalOrder + 1};
            }
            case Face::Right:
            {
                unsigned const startingIndex {(jElementY * numberOfElementsX + iElementX) * 4 * (numericalOrder + 1) + (numericalOrder + 1)};
                return std::span<Type const, numericalOrder + 1> {&values[startingIndex], numericalOrder + 1};
            }
            default:
                __builtin_unreachable();
        }
    };
    
private:

    unsigned numberOfElementsX, numberOfElementsY;
    
    std::vector<Type> values;
};

#endif
