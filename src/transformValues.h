#ifndef TRANSFORM_VALUES_H
#define TRANSFORM_VALUES_H

#include "points.h"
#include "pointValues2D.h"
#include "mesh2D.h"
#include "myVector.h"

template <bool inverse, typename Type, unsigned polynomialOrder>
void transformValues (pointValues2D<Type, polynomialOrder>& physicalValues, pointValues2D<Type, polynomialOrder>& transformedValues, mesh2D const& mesh)
{
    constexpr auto points {getPoints<polynomialOrder>()};
    
    for (unsigned iElementX = 0; iElementX < physicalValues.getNumberOfElementsX(); iElementX++)
    {
        for (unsigned iElementY = 0; iElementY < physicalValues.getNumberOfElementsY(); iElementY++)
        {
            for (unsigned jPointX = 0; jPointX <= polynomialOrder; jPointX++)
            {
                for (unsigned jPointY = 0; jPointY <= polynomialOrder; jPointY++)
                {
                    auto const jacobian {mesh.getJacobian(iElementX, iElementY, points[jPointX], points[jPointY])};
                    
                    if constexpr (inverse)
                        physicalValues(iElementX, iElementY, jPointX, jPointY) = transformedValues(iElementX, iElementY, jPointX, jPointY) / jacobian;
                    else
                        transformedValues(iElementX, iElementY, jPointX, jPointY) = physicalValues(iElementX, iElementY, jPointX, jPointY) * jacobian;
                }
            }
        }
    }
}

template <bool inverse, typename Type>
Type transformValue (Type const value, double const jacobian)
{
    if constexpr (inverse)
        return value / jacobian; // return physical value
    else
        return value * jacobian; // return transformed value
}

vector4D transformFluxF (vector4D const F, vector4D const G, double xDerivativeEta, double yDerivativeEta);

vector4D transformFluxG (vector4D const F, vector4D const G, double xDerivativeXi, double yDerivativeXi);

#endif
