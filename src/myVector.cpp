#include <cmath>

#include "myVector.h"

template <unsigned size>
myVector<size>& myVector<size>::operator= (const myVector<size>& a)
{
    for (unsigned index = 0; index < size; index++)
    {
        data[index] = a.data[index];
    }
    
    return *this;
}

template <unsigned size>
myVector<size>& myVector<size>::operator+= (const myVector<size>& a)
{
    for (unsigned index = 0; index < size; index++)
    {
        data[index] += a.data[index];
    }
    
    return *this;
}

template <unsigned size>
myVector<size>& myVector<size>::operator-= (const myVector<size>& a)
{
    for (unsigned index = 0; index < size; index++)
    {
        data[index] -= a.data[index];
    }
    
    return *this;
}

template <unsigned size>
myVector<size>& myVector<size>::operator*= (const double a)
{
    for (unsigned index = 0; index < size; index++)
    {
        data[index] *= a;
    }
    
    return *this;
}

template <unsigned size>
myVector<size>& myVector<size>::operator/= (const double a)
{
    for (unsigned index = 0; index < size; index++)
    {
        data[index] /= a;
    }
    
    return *this;
}

template <unsigned size>
double myVector<size>::norm () const
{
    double temp {};
    
    for (unsigned index = 0; index < size; index++)
    {
        temp += (data[index] * data[index]);
    }
    
    return std::sqrt(temp);
}

template <unsigned size>
void myVector<size>::normalize ()
{
    double norm {this->norm()};
    
    for (unsigned index = 0; index < size; index++)
    {
        data[index] /= norm;
    }
}

template <unsigned size>
double& myVector<size>::operator[] (const unsigned index)
{
    return data[index - 1];
}

template <unsigned size>
const double& myVector<size>::operator[] (const unsigned index) const
{
    return data[index - 1];
}

template <unsigned size>
myVector<size> operator+ (const myVector<size>& a, const myVector<size>& b)
{
    myVector<size> temp {};
    
    for (unsigned index = 0; index < size; index++)
    {
        temp[index + 1] = a[index + 1] + b[index + 1];
    }
    
    return temp;
}

template <unsigned size>
myVector<size> operator- (const myVector<size>& a, const myVector<size>& b)
{
    myVector<size> temp;
    
    for (unsigned index = 0; index < size; index++)
    {
        temp[index + 1] = a[index + 1] - b[index + 1];
    }
    
    return temp;
}

template <unsigned size>
myVector<size> operator* (const double a, const myVector<size>& b)
{
    myVector<size> temp;
    
    for (unsigned index = 0; index < size; index++)
    {
        temp[index + 1] = a * b[index + 1];
    }
    
    return temp;
}

template <unsigned size>
myVector<size> operator* (const myVector<size>& a, const double b)
{
    myVector<size> temp;
    
    for (unsigned index = 0; index < size; index++)
    {
        temp[index + 1] = a[index + 1] * b;
    }
    
    return temp;
}

template <unsigned size>
myVector<size> operator/ (const myVector<size>& a, const double b)
{
    myVector<size> temp;
    
    for (unsigned index = 0; index < size; index++)
    {
        temp[index + 1] = a[index + 1] / b;
    }
    
    return temp;
}

template <unsigned size>
double operator* (const myVector<size>& a, const myVector<size>& b)
{
    double temp {};
    
    for (unsigned index = 0; index < size; index++)
    {
        temp += a[index + 1] * b[index + 1];
    }
    
    return temp;
}

template <unsigned size>
double norm (myVector<size> const& a)
{
    return std::sqrt(a * a);
}

template <unsigned size>
myVector<size> normalize (myVector<size> a)
{
    double norm {a.norm()};
    
    for (unsigned index {1}; index <= size; index++)
    {
        a[index] /= norm;
    }
    
    return a;
}

vector2D orthoNormal (vector2D other)
{
    return normalize(vector2D(other.y(), -other.x()));
}

template class myVector<2u>;
template class myVector<3u>;
template class myVector<4u>;

template myVector<2u> operator+ (const myVector<2u>& a, const myVector<2u>& b);
template myVector<2u> operator- (const myVector<2u>& a, const myVector<2u>& b);
template myVector<2u> operator* (const double a, const myVector<2u>& b);
template myVector<2u> operator* (const myVector<2u>& a, const double b);
template myVector<2u> operator/ (const myVector<2u>& a, const double b);

template myVector<3u> operator+ (const myVector<3u>& a, const myVector<3u>& b);
template myVector<3u> operator- (const myVector<3u>& a, const myVector<3u>& b);
template myVector<3u> operator* (const double a, const myVector<3u>& b);
template myVector<3u> operator* (const myVector<3u>& a, const double b);
template myVector<3u> operator/ (const myVector<3u>& a, const double b);

template myVector<4u> operator+ (const myVector<4u>& a, const myVector<4u>& b);
template myVector<4u> operator- (const myVector<4u>& a, const myVector<4u>& b);
template myVector<4u> operator* (const double a, const myVector<4u>& b);
template myVector<4u> operator* (const myVector<4u>& a, const double b);
template myVector<4u> operator/ (const myVector<4u>& a, const double b);

template double operator* (const myVector<2u>& a, const myVector<2u>& b);

template double norm (myVector<2u> const& a);
template double norm (myVector<3u> const& a);

std::ostream& operator<< (std::ostream& os, vector2D const& vector)
{
    os << vector[1] << ' ' << vector[2];
    
    return os;
}

std::ostream& operator<< (std::ostream& os, vector3D const& vector)
{
    os << vector[1] << ' ' << vector[2] << ' ' << vector[3];
    
    return os;
}

std::ostream& operator<< (std::ostream& os, vector4D const& vector)
{
    os << vector[1] << ' ' << vector[2] << ' ' << vector[3] << ' ' << vector[4];
    
    return os;
}
