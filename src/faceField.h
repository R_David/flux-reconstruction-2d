#ifndef FACEFIELD_H_INCLUDED
#define FACEFIELD_H_INCLUDED

#include <vector>

#include "utility.h"

template <typename Type>
class faceField
{
public:
    
    faceField (unsigned numberOfElementsX_, unsigned numberOfElementsY_) :
        numberOfElementsX (numberOfElementsX_),
        numberOfElementsY (numberOfElementsY_)
    {
        valuesX.resize(numberOfElementsX * (numberOfElementsY + 1));
        valuesY.resize((numberOfElementsX + 1) * numberOfElementsY);
    };
    
    Type& operator() (unsigned iElementX, unsigned jElementY, Face facePosition)
    {
        switch (facePosition)
        {
            case Face::Top:
                return valuesX[((jElementY + 1) * numberOfElementsX + iElementX)];
            case Face::Bottom:
                return valuesX[(jElementY * numberOfElementsX + iElementX)];
            case Face::Left:
                return valuesY[(iElementX * numberOfElementsY + jElementY)];
            case Face::Right:
                return valuesY[((iElementX + 1) * numberOfElementsY + jElementY)];
            default:
                return valuesX.front();
        }
    };
    
    Type const& operator() (unsigned iElementX, unsigned jElementY, Face facePosition) const
    {
        switch (facePosition)
        {
            case Face::Top:
                return valuesX[((jElementY + 1) * numberOfElementsX + iElementX)];
            case Face::Bottom:
                return valuesX[(jElementY * numberOfElementsX + iElementX)];
            case Face::Left:
                return valuesY[(iElementX * numberOfElementsY + jElementY)];
            case Face::Right:
                return valuesY[((iElementX + 1) * numberOfElementsY + jElementY)];
            default:
                return valuesX.front();
        }
    };
    
private:
    
    unsigned numberOfElementsX, numberOfElementsY;
    
    std::vector<Type> valuesX;
    std::vector<Type> valuesY;
};

#endif // FACEFIELD_H_INCLUDED
