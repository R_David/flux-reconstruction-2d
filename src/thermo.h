#ifndef THERMO_H
#define THERMO_H

#include "myVector.h"

class thermo
{
public:
    thermo();
    ~thermo();
        
    static double getPressure (vector3D U);        
    static double getPressure (vector4D U);
    
    static double getEnergy (double rho, vector2D u, double p);
    
    static double getSpeedOfSound (vector4D U);
    static double getMachNumber (vector4D U);

    static double getEntropy (vector4D U);
    
    static constexpr double kappa () {return 1.4;};
    static constexpr double cp () {return 1005;};
    static constexpr double cv () {return cp() / r();};
    static constexpr double r () {return 287.1;};
        
private:
    
    double kappa_;
    double cp_;
    double cv_;
    double r_;
};

#endif
