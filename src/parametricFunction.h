#ifndef PARAMETRIC_FUNCTION_H
#define PARAMETRIC_FUNCTION_H

#include "myVector.h"

using parametricFunction = auto (*)(double u, double v) -> point2D;

#endif
