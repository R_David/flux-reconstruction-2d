#ifndef HLLC_H
#define HLLC_H

#include "myVector.h"

// input and output is in physical values
vector4D computeHLLCFlux (vector4D const& ULeft, vector4D const& URight, vector2D const normalVector);

#endif
