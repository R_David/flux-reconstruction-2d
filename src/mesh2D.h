#ifndef MESH_2D_H
#define MESH_2D_H

#include "border.h"
#include "faceField.h"
#include "myVector.h"
#include "staticMatrix.h"
#include "vertexField.h"
#include "volField.h"

enum class Border
{
    Top,
    Bottom,
    Left,
    Right
};

class mesh2D
{
public:
    
    mesh2D (unsigned const numberOfElementsX_, unsigned const numberOfElementsY_);
    
    vertexField<point2D> const& getVertexCoordinates () const;
    
    double getJacobian (unsigned iElementX, unsigned jElementY, double xi, double eta) const;

    staticMatrix<2, 2> getJacobianMatrix (unsigned iElementX, unsigned jElementY, double xi, double eta) const;
    staticMatrix<2, 2> getInverseJacobianMatrix (unsigned iElementX, unsigned jElementY, double xi, double eta) const;
    
    unsigned getNumberOfElementsX () const;
    
    unsigned getNumberOfElementsY () const;
    
    bool createMesh (bool readMeshFromFile, std::string const& meshFile = std::string{});
    
    void setBorders (border leftBorder_, border rightBorder_, border topBorder_, border bottomBorder_);
    
    double getXDerivativeXi (unsigned iElementX, unsigned jElementY, double xi, double eta) const;
    double getXDerivativeEta (unsigned iElementX, unsigned jElementY, double xi, double eta) const;
    double getYDerivativeXi (unsigned iElementX, unsigned jElementY, double xi, double eta) const;
    double getYDerivativeEta (unsigned iElementX, unsigned jElementY, double xi, double eta) const;
    
    point2D getCoordinate (unsigned iElementX, unsigned jElementY, double xi, double eta) const;
    
    vector2D const getNormalVector (unsigned iElementX, unsigned jElementY, Face face) const;
    
    vector2D const getBoundaryFaceNormalVector (unsigned iElement, double parameter, Border border) const;
    
    vector2D getSizeVectorXi (unsigned iElementX, unsigned jElementY) const;
    vector2D getSizeVectorEta (unsigned iElementX, unsigned jElementY) const;
    
private:
    
    unsigned numberOfElementsX {};
    unsigned numberOfElementsY {};
    
    vertexField<point2D> vertexCoordinates;
    
    faceField<vector2D> normalVectors;
    
    volField<vector2D> sizeVectorsXi;
    volField<vector2D> sizeVectorsEta;
    
    border leftBorder;
    border rightBorder;
    border topBorder;
    border bottomBorder;
};

#endif
