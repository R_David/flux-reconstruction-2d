#include "mesh2D.h"
#include "myVector.h"

#include <cmath>
#include <fstream>

mesh2D::mesh2D (unsigned const numberOfElementsX_, unsigned const numberOfElementsY_) :
    numberOfElementsX(numberOfElementsX_),
    numberOfElementsY(numberOfElementsY_),
    vertexCoordinates(numberOfElementsX, numberOfElementsY),
    normalVectors(numberOfElementsX, numberOfElementsY),
    sizeVectorsXi(numberOfElementsX, numberOfElementsY),
    sizeVectorsEta(numberOfElementsX, numberOfElementsY)
{
}

vertexField<point2D> const& mesh2D::getVertexCoordinates () const
{
    return vertexCoordinates;
}

unsigned mesh2D::getNumberOfElementsX () const
{
    return numberOfElementsX;
}

unsigned mesh2D::getNumberOfElementsY () const
{
    return numberOfElementsY;
}

void mesh2D::setBorders (border leftBorder_, border rightBorder_, border topBorder_, border bottomBorder_)
{
    leftBorder = leftBorder_;
    rightBorder = rightBorder_;
    topBorder = topBorder_;
    bottomBorder = bottomBorder_;
}

bool mesh2D::createMesh (bool readMeshFromFile, std::string const& meshFile)
{
    enum class Direction
    {
        Horizontal,
        Vertical
    };
    
    Direction direction = Direction::Horizontal;

    if (readMeshFromFile)
    {
        std::ifstream meshFileStream(meshFile);
        
        if (meshFileStream.is_open())
        {
            int numberOfVerticesX = numberOfElementsX + 1;
            int numberOfVerticesY = numberOfElementsY + 1;
            
            for (int jVertex = numberOfVerticesY - 1; jVertex >= 0 ; jVertex--)
            {
                for (int iVertex = numberOfVerticesX - 1; iVertex >= 0; iVertex--)
                {
                    meshFileStream >> vertexCoordinates(iVertex, jVertex).x() >> vertexCoordinates(iVertex, jVertex).y();
                }
            }
            
            // int numberOfVerticesX;
            // int numberOfVerticesY;
            
            // meshFile >> numberOfVerticesX;
            // meshFile >> numberOfVerticesY;

            // for (int jVertex = 0; jVertex < numberOfVerticesY; jVertex++)
            // {
            //     for (int iVertex = numberOfVerticesX - 1; iVertex >= 0; iVertex--)
            //     {
            //         meshFile >> vertexCoordinates(iVertex, jVertex).x();
            //     }
            // }
            
            // for (int jVertex = 0; jVertex < numberOfVerticesY; jVertex++)
            // {
            //     for (int iVertex = numberOfVerticesX - 1; iVertex >= 0; iVertex--)
            //     {
            //         meshFile >> vertexCoordinates(iVertex, jVertex).y();
            //     }
            // }
        }
        else
        {
            std::cout << "Mesh file not found!\n";
            
            return false;
        }
    }
    else
    {
        // boundary vertices
        for (unsigned iElementX = 0; iElementX <= numberOfElementsX; iElementX++)
        {
            vertexCoordinates(iElementX, 0) = bottomBorder(static_cast<double>(iElementX) / numberOfElementsX, 0.);
            vertexCoordinates(iElementX, numberOfElementsY) = topBorder(static_cast<double>(iElementX) / numberOfElementsX, 1.);
        }
        
        for (unsigned jElementY = 1; jElementY <= numberOfElementsY; jElementY++)
        {
            vertexCoordinates(0, jElementY) = leftBorder(0., static_cast<double>(jElementY) / numberOfElementsY);
            vertexCoordinates(numberOfElementsX, jElementY) = rightBorder(1., static_cast<double>(jElementY) / numberOfElementsY);
        }
        
        // inner vertices
        if (direction == Direction::Vertical)
        {
            for (unsigned iElementX = 1; iElementX < numberOfElementsX; iElementX++)
            {
                for (unsigned jElementY = 1; jElementY < numberOfElementsY; jElementY++)
                {
                    vertexCoordinates(iElementX, jElementY) = vertexCoordinates(iElementX, 0) + vector2D(vertexCoordinates(iElementX, numberOfElementsY), vertexCoordinates(iElementX, 0)) * jElementY / numberOfElementsY;
                }
            }
        }
        else
        {
            for (unsigned jElementY = 1; jElementY < numberOfElementsY; jElementY++)
            {
                for (unsigned iElementX = 1; iElementX < numberOfElementsX; iElementX++)
                {
                    vertexCoordinates(iElementX, jElementY) = vertexCoordinates(0, jElementY) + vector2D(vertexCoordinates(numberOfElementsX, jElementY), vertexCoordinates(0, jElementY)) * iElementX / numberOfElementsX;
                }
            }
        }
    }
    
    // normal vectors
    for (unsigned iElementX {0}; iElementX < numberOfElementsX; iElementX++)
    {
        for (unsigned jElementY {0}; jElementY < numberOfElementsY; jElementY++)
        {
            normalVectors(iElementX, jElementY, Face::Top) = orthoNormal(vector2D(vertexCoordinates(iElementX, jElementY, Vertex::TopLeft),
                                                                                  vertexCoordinates(iElementX, jElementY, Vertex::TopRight)));
            
            normalVectors(iElementX, jElementY, Face::Right) = orthoNormal(vector2D(vertexCoordinates(iElementX, jElementY, Vertex::TopRight),
                                                                                    vertexCoordinates(iElementX, jElementY, Vertex::BottomRight)));
        }
    }
    
    for (unsigned iElementX {0}; iElementX < numberOfElementsX; iElementX++)
    {
        normalVectors(iElementX, 0, Face::Bottom) = -1 * orthoNormal(vector2D(vertexCoordinates(iElementX, 0, Vertex::BottomRight),
                                                                              vertexCoordinates(iElementX, 0, Vertex::BottomLeft)));
    }
    
    for (unsigned jElementY {0}; jElementY < numberOfElementsY; jElementY++)
    {
        normalVectors(0, jElementY, Face::Left) = -1 * orthoNormal(vector2D(vertexCoordinates(0, jElementY, Vertex::BottomLeft),
                                                                            vertexCoordinates(0, jElementY, Vertex::TopLeft)));
    }
    
    for (unsigned iElementX {0}; iElementX < numberOfElementsX; iElementX++)
    {
        for (unsigned jElementY {0}; jElementY < numberOfElementsY; jElementY++)
        {
            sizeVectorsXi(iElementX, jElementY) = vector2D((vertexCoordinates(iElementX, jElementY, Vertex::TopLeft) +
                                                            vertexCoordinates(iElementX, jElementY, Vertex::BottomLeft)) / 2.,
                                                           (vertexCoordinates(iElementX, jElementY, Vertex::TopRight) +
                                                            vertexCoordinates(iElementX, jElementY, Vertex::BottomRight)) / 2);
            
            sizeVectorsEta(iElementX, jElementY) = vector2D((vertexCoordinates(iElementX, jElementY, Vertex::TopLeft) +
                                                             vertexCoordinates(iElementX, jElementY, Vertex::TopRight)) / 2.,
                                                            (vertexCoordinates(iElementX, jElementY, Vertex::BottomLeft) +
                                                             vertexCoordinates(iElementX, jElementY, Vertex::BottomRight)) / 2);
        }
    }
    
    return true;
}

double mesh2D::getXDerivativeXi (unsigned iElementX, unsigned jElementY, double xi, double eta) const
{
    return (1 - eta) / 4. * (vertexCoordinates(iElementX, jElementY, Vertex::BottomRight).x() - vertexCoordinates(iElementX, jElementY, Vertex::BottomLeft).x()) +
           (1 + eta) / 4. * (vertexCoordinates(iElementX, jElementY, Vertex::TopRight).x() - vertexCoordinates(iElementX, jElementY, Vertex::TopLeft).x());
}

double mesh2D::getXDerivativeEta (unsigned iElementX, unsigned jElementY, double xi, double eta) const
{
    return (1 - xi) / 4. * (vertexCoordinates(iElementX, jElementY, Vertex::TopLeft).x() - vertexCoordinates(iElementX, jElementY, Vertex::BottomLeft).x()) +
           (1 + xi) / 4. * (vertexCoordinates(iElementX, jElementY, Vertex::TopRight).x() - vertexCoordinates(iElementX, jElementY, Vertex::BottomRight).x());
}

double mesh2D::getYDerivativeXi (unsigned iElementX, unsigned jElementY, double xi, double eta) const
{
    return (1 - eta) / 4. * (vertexCoordinates(iElementX, jElementY, Vertex::BottomRight).y() - vertexCoordinates(iElementX, jElementY, Vertex::BottomLeft).y()) +
           (1 + eta) / 4. * (vertexCoordinates(iElementX, jElementY, Vertex::TopRight).y() - vertexCoordinates(iElementX, jElementY, Vertex::TopLeft).y());
}

double mesh2D::getYDerivativeEta (unsigned iElementX, unsigned jElementY, double xi, double eta) const
{
    return (1 - xi) / 4. * (vertexCoordinates(iElementX, jElementY, Vertex::TopLeft).y() - vertexCoordinates(iElementX, jElementY, Vertex::BottomLeft).y()) +
           (1 + xi) / 4. * (vertexCoordinates(iElementX, jElementY, Vertex::TopRight).y() - vertexCoordinates(iElementX, jElementY, Vertex::BottomRight).y());
}

staticMatrix<2, 2> mesh2D::getJacobianMatrix (unsigned iElementX, unsigned jElementY, double xi, double eta) const
{
    staticMatrix<2, 2> result;
    
    result(1, 1) = getXDerivativeXi(iElementX, jElementY, xi, eta);
    result(1, 2) = getXDerivativeEta(iElementX, jElementY, xi, eta);
    result(2, 1) = getYDerivativeXi(iElementX, jElementY, xi, eta);
    result(2, 2) = getYDerivativeEta(iElementX, jElementY, xi, eta);
    
    return result;
}

staticMatrix<2, 2> mesh2D::getInverseJacobianMatrix (unsigned iElementX, unsigned jElementY, double xi, double eta) const
{
    auto jacobianMatrix {getJacobianMatrix(iElementX, jElementY, xi, eta)};
    
    staticMatrix<2, 2> result;
    
    result(1, 1) = jacobianMatrix(2, 2);
    result(1, 2) = -jacobianMatrix(1, 2);
    result(2, 1) = -jacobianMatrix(2, 1);
    result(2, 2) = jacobianMatrix(1, 1);
    
    result *= (1. / getJacobian(iElementX, jElementY, xi, eta));
    
    return result;
}

double mesh2D::getJacobian (unsigned iElementX, unsigned jElementY, double xi, double eta) const
{
    return getXDerivativeXi(iElementX, jElementY, xi, eta) *
           getYDerivativeEta(iElementX, jElementY, xi, eta) -
           getXDerivativeEta(iElementX, jElementY, xi, eta) *
           getYDerivativeXi(iElementX, jElementY, xi, eta);
}

point2D mesh2D::getCoordinate (unsigned iElementX, unsigned jElementY, double xi, double eta) const
{
    return vertexCoordinates(iElementX, jElementY, Vertex::BottomLeft) * (1 - xi) * (1 - eta) / 4. +
           vertexCoordinates(iElementX, jElementY, Vertex::BottomRight) * (1 + xi) * (1 - eta) / 4. +
           vertexCoordinates(iElementX, jElementY, Vertex::TopLeft) * (1 - xi) * (1 + eta) / 4. +
           vertexCoordinates(iElementX, jElementY, Vertex::TopRight) * (1 + xi) * (1 + eta) / 4.;
}

vector2D const mesh2D::getBoundaryFaceNormalVector (unsigned iElement, double parameter, Border border) const
{
    switch (border)
    {
        case Border::Left:
        {
            auto const coordinate {getCoordinate(0, iElement, -1., parameter)};
            
            auto const parametricCoordinate {leftBorder.parametricCoordinate(coordinate.x(), coordinate.y())};
            
            auto const epsilonMinus {leftBorder(0., parametricCoordinate.y() - 0.0005)};
            auto const epsilonPlus {leftBorder(0., parametricCoordinate.y() + 0.0005)};
            
            return orthoNormal(vector2D(epsilonMinus, epsilonPlus));
        }
        case Border::Right:
        {
            auto const coordinate {getCoordinate(numberOfElementsY - 1, iElement, 1., parameter)};
            
            auto const parametricCoordinate {rightBorder.parametricCoordinate(coordinate.x(), coordinate.y())};
            
            auto const epsilonMinus {rightBorder(1., parametricCoordinate.y() - 0.0005)};
            auto const epsilonPlus {rightBorder(1., parametricCoordinate.y() + 0.0005)};
            
            return orthoNormal(vector2D(epsilonPlus, epsilonMinus));
        }
        case Border::Top:
        {
            if (iElement < wallBeginning)
                return {0., -1.};
            
            if (iElement >= wallEnd)
                return {0., 1.};
            
            if (iElement == wallBeginning or iElement == wallEnd - 1)
            {
                return getNormalVector(iElement, numberOfElementsY - 1, Face::Top);
            }
            
            auto const x {getCoordinate(iElement, numberOfElementsY - 1, parameter, 1.).x()};
            
            auto const derivative {0.12 * (0.74225 * 1 / std::sqrt(x) - 0.63 - 3.516 * x + 4.2645 * x * x - 2.03 * x * x * x)};
            
            double y;
            
            if (iElement < (numberOfElementsX / 2))
                y = -1;
            else
                y = 1;
            
            vector2D normalVector {derivative, y};
            
            normalVector.normalize();
            
            return normalVector;
            
            // auto const coordinate {getCoordinate(iElement, numberOfElementsY - 1, parameter, 1.)};
            
            // auto const parametricCoordinate {topBorder.parametricCoordinate(coordinate.x(), coordinate.y())};
            
            // auto const epsilonMinus {topBorder(parametricCoordinate.x() - 0.0005, 1.)};
            // auto const epsilonPlus {topBorder(parametricCoordinate.x() + 0.0005, 1.)};
            
            // return orthoNormal(vector2D(epsilonMinus, epsilonPlus));
        }
        case Border::Bottom:
        {
            auto const coordinate {getCoordinate(iElement, 0, parameter, -1.)};
            
            auto const parametricCoordinate {bottomBorder.parametricCoordinate(coordinate.x(), coordinate.y())};
            
            auto derivative {bottomBorder.getDerivative(parametricCoordinate.x(), parametricCoordinate.y())};
            derivative.normalize();
            
            return derivative;
        }
        default:
            return {0., 0.};
    }
}

vector2D const mesh2D::getNormalVector (unsigned iElementX, unsigned jElementY, Face face) const
{
    switch (face)
    {
        case Face::Top:
        case Face::Right:
            return normalVectors(iElementX, jElementY, face);
            
        case Face::Bottom:
        case Face::Left:
            return -1. * normalVectors(iElementX, jElementY, face);
        
        default:
            return normalVectors(0, 0, Face::Top);
    }
}

vector2D mesh2D::getSizeVectorXi (unsigned iElementX, unsigned jElementY) const
{
    return sizeVectorsXi(iElementX, jElementY);
}

vector2D mesh2D::getSizeVectorEta (unsigned iElementX, unsigned jElementY) const
{
    return sizeVectorsEta(iElementX, jElementY);
}
