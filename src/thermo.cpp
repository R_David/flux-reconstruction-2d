#include <cmath>

#include "thermo.h"

double thermo::getPressure (vector3D U)
{
    return ((thermo::kappa() - 1) * (U[3] - 1. / 2. * U[2] * U[2] / U[1]));
}

double thermo::getPressure (vector4D U)
{
    vector2D u {U[2] / U[1], U[3] / U[1]};
    
    return ((thermo::kappa() - 1) * (U[4] - 1. / 2. * U[1] * (u * u)));
}

double thermo::getEnergy (double rho, vector2D u, double p)
{
    return p / (thermo::kappa() - 1) + 0.5 * rho * u * u;
}

double thermo::getSpeedOfSound (vector4D U)
{
    auto const pressure {thermo::getPressure(U)};
    
    return std::sqrt(thermo::kappa() * pressure / U[1]);
}

double thermo::getMachNumber (vector4D U)
{
    vector2D u {U[2] / U[1], U[3] / U[1]};
    
    auto const uMag {std::sqrt(u * u)};
    
    return uMag / thermo::getSpeedOfSound(U);
}

double thermo::getEntropy (vector4D U)
{
    auto const p {getPressure(U)};
    
    return p * std::pow(U[1], -kappa());
}
