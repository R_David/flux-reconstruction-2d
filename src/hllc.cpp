#include "hllc.h"

#include <cmath>
#include <iostream>

#include "myVector.h"
#include "thermo.h"

vector4D computeHLLCFlux (vector4D const& ULeft, vector4D const& URight, vector2D const normalVector)
{
    double const rhoLeft {ULeft[1]};
    double const rhoRight {URight[1]};
    
    double const eLeft {ULeft[4]};
    double const eRight {URight[4]};
    
    double const pLeft {thermo::getPressure(ULeft)};
    double const pRight {thermo::getPressure(URight)};
    
    double const cLeft {std::sqrt((thermo::kappa() * pLeft / rhoLeft))};
    double const cRight {std::sqrt((thermo::kappa() * pRight / rhoRight))};
    
    double const Rp {std::sqrt(rhoRight / rhoLeft)};
    
    vector2D const uLeft {ULeft[2] / ULeft[1], ULeft[3] / ULeft[1]};
    vector2D const uRight {URight[2] / URight[1], URight[3] / URight[1]};
    
    vector2D const uTilde {(uLeft + Rp * uRight) / (1 + Rp)};
    
    double const HLeft {(eLeft + pLeft) / rhoLeft};
    double const HRight {(eRight + pRight) / rhoRight};
    
    double const HTilde {(HLeft + Rp * HRight) / (1 + Rp)};
    
    double const cTilde {std::sqrt((thermo::kappa() - 1.) * (HTilde - 0.5 * uTilde * uTilde))};
    
    double const qTilde {uTilde * normalVector};
    
    double const qLeft {uLeft * normalVector};
    double const qRight {uRight * normalVector};
    
    double const SLeft {std::min(qLeft - cLeft, qTilde - cTilde)};
    double const SRight {std::max(qRight + cRight, qTilde + cTilde)};
    
    double const SMiddle {(rhoRight * qRight * (SRight - qRight) - rhoLeft * qLeft * (SLeft - qLeft) + pLeft - pRight) / (rhoRight * (SRight - qRight) - rhoLeft * (SLeft - qLeft))};
    
    double const pStar {rhoLeft * (qLeft - SLeft) * (qLeft - SMiddle) + pLeft};
    
    if (SLeft > 0.)
    {
        return vector4D(rhoLeft * qLeft, rhoLeft * uLeft.x() * qLeft + pLeft * normalVector.x(), rhoLeft * uLeft.y() * qLeft + pLeft * normalVector.y(), (eLeft + pLeft) * qLeft);
    }
    else if (SLeft <= 0. and SMiddle > 0.)
    {
        double const rhoStar {rhoLeft * (SLeft - qLeft) / (SLeft - SMiddle)};
        double const rhoUStar {((SLeft - qLeft) * rhoLeft * uLeft.x() + (pStar - pLeft) * normalVector.x()) / (SLeft - SMiddle)};
        double const rhoVStar {((SLeft - qLeft) * rhoLeft * uLeft.y() + (pStar - pLeft) * normalVector.y()) / (SLeft - SMiddle)};
        double const eStar {((SLeft - qLeft) * eLeft - pLeft * qLeft + pStar * SMiddle) / (SLeft - SMiddle)};
        
        return vector4D(rhoStar * SMiddle, rhoUStar * SMiddle + pStar * normalVector.x(), rhoVStar * SMiddle + pStar * normalVector.y(), (eStar + pStar) * SMiddle);
    }
    else if (SMiddle <= 0. and SRight >= 0.)
    {
        double const rhoStar {rhoRight * (SRight - qRight) / (SRight - SMiddle)};
        double const rhoUStar {((SRight - qRight) * rhoRight * uRight.x() + (pStar - pRight) * normalVector.x()) / (SRight - SMiddle)};
        double const rhoVStar {((SRight - qRight) * rhoRight * uRight.y() + (pStar - pRight) * normalVector.y()) / (SRight - SMiddle)};
        double const eStar {((SRight - qRight) * eRight - pRight * qRight + pStar * SMiddle) / (SRight - SMiddle)};
        
        return vector4D(rhoStar * SMiddle, rhoUStar * SMiddle + pStar * normalVector.x(), rhoVStar * SMiddle + pStar * normalVector.y(), (eStar + pStar) * SMiddle);
    }
    else
    {
        return vector4D(rhoRight * qRight, rhoRight * uRight.x() * qRight + pRight * normalVector.x(), rhoRight * uRight.y() * qRight + pRight * normalVector.y(), (eRight + pRight) * qRight);
    }
}
