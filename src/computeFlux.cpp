#include "thermo.h"
#include "transformValues.h"
#include "utilityFunctions.h"

vector4D computeFluxF (vector4D const UTransformed, mesh2D const& mesh, double xi, double eta, unsigned iElementX, unsigned jElementY)
{
    // compute physical values
    vector4D U {transformValue<true>(UTransformed, mesh.getJacobian(iElementX, jElementY, xi, eta))};
    double p {thermo::getPressure(U)};
    
    vector4D F {U[2], U[2] * U[2] / U[1] + p, U[2] * U[3] / U[1], (U[4] + p) * U[2] / U[1]};
    
    vector4D G {U[3], U[2] * U[3] / U[1], U[3] * U[3] / U[1] + p, (U[4] + p) * U[3] / U[1]};
    
    return mesh.getYDerivativeEta(iElementX, jElementY, xi, eta) * F - mesh.getXDerivativeEta(iElementX, jElementY, xi, eta) * G;
}

// TODO change input to QXi and QEta
vector4D computeFluxFv (vector4D const UTransformed, vector4D const QX, vector4D const QY, mesh2D const& mesh, double xi, double eta, unsigned iElementX, unsigned jElementY)
{
    // compute physical values
    vector4D U {transformValue<true>(UTransformed, mesh.getJacobian(iElementX, jElementY, xi, eta))};
    
    auto const Fv {computeFluxFvPhysical(U, QX, QY)};
    auto const Gv {computeFluxGvPhysical(U, QX, QY)};
    
    return mesh.getYDerivativeEta(iElementX, jElementY, xi, eta) * Fv - mesh.getXDerivativeEta(iElementX, jElementY, xi, eta) * Gv;
}

vector4D computeFluxFvPhysical (vector4D const U, vector4D const QX, vector4D const QY)
{
    auto const rho {U[1]};
    
    auto const u {U[2] / U[1]};
    auto const v {U[3] / U[1]};
    
    auto const DuDX {1. / rho * (QX[2] - u * QX[1])};
    auto const DuDY {1. / rho * (QY[2] - u * QY[1])};
    
    auto const DvDX {1. / rho * (QX[3] - v * QX[1])};
    auto const DvDY {1. / rho * (QY[3] - v * QY[1])};
    
    auto const DpDX {(kappa - 1.) * (QX[4] - 0.5 * QX[1] * (u * u + v * v) - rho * (u * DuDX + v * DvDX))};
    
    auto const p {(kappa - 1.) * (U[4] - 0.5 * rho * (u * u + v * v))};
    
    auto const DprhoDX {DpDX / rho - QX[1] * p / (rho * rho)};
    
    vector4D Fv {0,
                 mu * (4. / 3. * DuDX - 2. / 3. * DvDY),
                 mu * (DuDY + DvDX),
                 mu * u * (4. / 3. * DuDX - 2. / 3. * DvDY) + mu * v * (DuDY + DvDX) + kappa /  (kappa - 1) * mu / Pr * DprhoDX};
    
    return Fv;
}

vector4D computeFluxG (vector4D const UTransformed, mesh2D const& mesh, double xi, double eta, unsigned iElementX, unsigned jElementY)
{
    // compute physical values
    vector4D U {transformValue<true>(UTransformed, mesh.getJacobian(iElementX, jElementY, xi, eta))};
    double p {thermo::getPressure(U)};
    
    vector4D F {U[2], U[2] * U[2] / U[1] + p, U[2] * U[3] / U[1], (U[4] + p) * U[2] / U[1]};
    
    vector4D G {U[3], U[2] * U[3] / U[1], U[3] * U[3] / U[1] + p, (U[4] + p) * U[3] / U[1]};
    
    return -1 * mesh.getYDerivativeXi(iElementX, jElementY, xi, eta) * F + mesh.getXDerivativeXi(iElementX, jElementY, xi, eta) * G;
}

vector4D computeFluxGv (vector4D const UTransformed, vector4D const QX, vector4D const QY, mesh2D const& mesh, double xi, double eta, unsigned iElementX, unsigned jElementY)
{
    vector4D U {transformValue<true>(UTransformed, mesh.getJacobian(iElementX, jElementY, xi, eta))};
    
    auto const Fv {computeFluxFvPhysical(U, QX, QY)};
    auto const Gv {computeFluxGvPhysical(U, QX, QY)};
    
    return -1 * mesh.getYDerivativeXi(iElementX, jElementY, xi, eta) * Fv + mesh.getXDerivativeXi(iElementX, jElementY, xi, eta) * Gv;
}

vector4D computeFluxGvPhysical (vector4D const U, vector4D const QX, vector4D const QY)
{
    auto const rho {U[1]};
    
    auto const u {U[2] / U[1]};
    auto const v {U[3] / U[1]};
    
    auto const DuDX {1. / rho * (QX[2] - u * QX[1])};
    auto const DuDY {1. / rho * (QY[2] - u * QY[1])};
    
    auto const DvDX {1. / rho * (QX[3] - v * QX[1])};
    auto const DvDY {1. / rho * (QY[3] - v * QY[1])};
    
    auto const DpDY {(kappa - 1.) * (QY[4] - 0.5 * QY[1] * (u * u + v * v) - rho * (u * DuDY + v * DvDY))};
    
    auto const p {(kappa - 1.) * (U[4] - 0.5 * rho * (u * u + v * v))};
    
    auto const DprhoDY {DpDY / rho - QY[1] * p / (rho * rho)};
    
    vector4D Gv {0,
                 mu * (DuDY + DvDX),
                 mu * (4. / 3. * DvDY - 2. / 3. * DuDX),
                 mu * u * (DuDY + DvDX) + mu * v * (4. / 3. * DvDY - 2. / 3. * DuDX) + kappa /  (kappa - 1) * mu / Pr * DprhoDY};
    
    return Gv;
}
