#ifndef BOUNDARY_CONDITIONS_H
#define BOUNDARY_CONDITIONS_H

#include <cmath>

#include "borderValues2D.h"
#include "dualFaceValues2D.h"
#include "mesh2D.h"
#include "myVector.h"
#include "points.h"
#include "thermo.h"

template <unsigned polynomialOrder>
void applyBoundaryConditions (borderValues2D<vector4D, polynomialOrder>& leftBorderFaceValues,
                              borderValues2D<vector4D, polynomialOrder>& rightBorderFaceValues,
                              borderValues2D<vector4D, polynomialOrder>& topBorderFaceValues,
                              borderValues2D<vector4D, polynomialOrder>& bottomBorderFaceValues,
                              dualFaceValues2D<vector4D, polynomialOrder> const& innerFaceValues,
                              mesh2D const& mesh)
{
    constexpr auto points {getPoints<polynomialOrder>()};
    
    for (unsigned iElementX = 0; iElementX < mesh.getNumberOfElementsX(); iElementX++)
    {
        for (unsigned index = 0; index <= polynomialOrder; index++)
        {
            // top
            {
                // interface
                if (iElementX < wallBeginning or iElementX >= wallEnd)
                {
                    auto const otherInterfaceValue {innerFaceValues(mesh.getNumberOfElementsX() - iElementX - 1,
                                                                    mesh.getNumberOfElementsY() - 1, Face::Top, polynomialOrder - index)};
                    
                    auto const otherJacobian {mesh.getJacobian(mesh.getNumberOfElementsX() - iElementX - 1, mesh.getNumberOfElementsY() - 1, points[polynomialOrder - index], 1.)};
                    
                    auto const otherInterfaceValuePhysical {transformValue<true>(otherInterfaceValue, otherJacobian)};
                    
                    topBorderFaceValues(iElementX, index) = otherInterfaceValuePhysical;
                }
                // wall
                else
                {
                    auto const interfaceValue {innerFaceValues(iElementX, mesh.getNumberOfElementsY() - 1, Face::Top, index)};
                    
                    auto const UInnerPhysical {transformValue<true>(interfaceValue,
                                                                    mesh.getJacobian(iElementX, mesh.getNumberOfElementsY() - 1, points[index], 1.))};
                    
                    // TODO
                    auto const normalVector {mesh.getBoundaryFaceNormalVector(iElementX, points[index], Border::Top)};
                    //auto const normalVector {mesh.getNormalVector(iElementX, mesh.getNumberOfElementsY() - 1, Face::Top)};
                    
                    auto const rho {UInnerPhysical[1]};
                    
                    vector2D const uInner {UInnerPhysical[2] / rho, UInnerPhysical[3] / rho};
                    
                    auto const uNorm {uInner * normalVector};
                    
                    topBorderFaceValues(iElementX, index)[1] = UInnerPhysical[1];
                    topBorderFaceValues(iElementX, index)[2] = UInnerPhysical[2] - 2 * rho * uNorm * normalVector.x();
                    topBorderFaceValues(iElementX, index)[3] = UInnerPhysical[3] - 2 * rho * uNorm * normalVector.y();
                    // topBorderFaceValues(iElementX, index)[2] = -1 * UInnerPhysical[2];
                    // topBorderFaceValues(iElementX, index)[3] = -1 * UInnerPhysical[3];
                    topBorderFaceValues(iElementX, index)[4] = UInnerPhysical[4];
                }
                
                // auto const UInnerPhysical {transformValue<true>(innerFaceValues(iElementX, mesh.getNumberOfElementsY() - 1, Face::Top, index),
                //                                                 mesh.getJacobian(iElementX, mesh.getNumberOfElementsY() - 1, points[index], 1.))};
                
                // auto const normalVector {mesh.getBoundaryFaceNormalVector(iElementX, points[index], Border::Top)};
                
                // // double const pOut {17.857};
                
                // double const rho {UInnerPhysical[1]};
                
                // vector2D const u {vector2D{UInnerPhysical[2] / rho, UInnerPhysical[3] / rho}};
                
                // // auto const pIn {thermo::getPressure(UInnerPhysical)};
                
                // vector2D uInner = {UInnerPhysical[2] / UInnerPhysical[1], UInnerPhysical[3] / UInnerPhysical[1]};
                
                // auto const uNorm = uInner * normalVector;
                
                // topBorderFaceValues(iElementX, index)[1] = UInnerPhysical[1];
                // topBorderFaceValues(iElementX, index)[2] = UInnerPhysical[2] - 2 * rho * uNorm * normalVector.x();
                // topBorderFaceValues(iElementX, index)[3] = UInnerPhysical[3] - 2 * rho * uNorm * normalVector.y();
                // topBorderFaceValues(iElementX, index)[4] = UInnerPhysical[4];
                // topBorderFaceValues(iElementX, index)[4] = thermo::getEnergy(rho, u, 2 * pOut - pIn);
            }
            
            // bottom
            {
                // inlet
                {
                    auto const jacobian {mesh.getJacobian(iElementX, 0, points[index], -1.)};
                    
                    auto const UInnerPhysical {transformValue<true>(innerFaceValues(iElementX, 0, Face::Bottom, index), jacobian)};
                    
                    double const pIn {std::min(thermo::getPressure(UInnerPhysical), p0)};
                    
                    double const mach {std::sqrt(2. / (thermo::kappa() - 1) * (std::pow(p0 / pIn, (thermo::kappa() - 1) / thermo::kappa()) - 1))};
                    
                    double rho {std::pow(pIn / p0, 1 / thermo::kappa()) * rho0};
                    
                    double const c {std::sqrt(thermo::kappa() * pIn / rho)};
                    
                    double const AoA {AoADeg * std::numbers::pi / 180.};
                    
                    bottomBorderFaceValues(iElementX, index)[1] = rho;
                    bottomBorderFaceValues(iElementX, index)[2] = rho * mach * c * std::cos(AoA);
                    bottomBorderFaceValues(iElementX, index)[3] = rho * mach * c * std::sin(AoA);
                    bottomBorderFaceValues(iElementX, index)[4] = thermo::getEnergy(rho, vector2D{mach * c, 0.}, pIn);
                }
                
                // auto const UInnerPhysical {transformValue<true>(innerFaceValues(iElementX, 0, Face::Bottom, index), mesh.getJacobian(iElementX, 0, points[index], -1.))};
                
                // auto const normalVector {mesh.getBoundaryFaceNormalVector(iElementX, points[index], Border::Bottom)};
                
                // bottomBorderFaceValues(iElementX, index)[1] = UInnerPhysical[1];
                // bottomBorderFaceValues(iElementX, index)[4] = UInnerPhysical[4];
                
                // if (iElementX < mesh.getNumberOfElementsX() / 2)
                // {
                //     vector2D uInner = {UInnerPhysical[2] / UInnerPhysical[1], UInnerPhysical[3] / UInnerPhysical[1]};
                    
                //     auto const UNorm = uInner * normalVector;
                    
                //     bottomBorderFaceValues(iElementX, index)[2] = UInnerPhysical[2] - 2 * UInnerPhysical[1] * UNorm * normalVector.x();
                //     bottomBorderFaceValues(iElementX, index)[3] = UInnerPhysical[3] - 2 * UInnerPhysical[1] * UNorm * normalVector.y();
                // }
                // else
                // {
                //     bottomBorderFaceValues(iElementX, index)[2] = -1. * UInnerPhysical[2];
                //     bottomBorderFaceValues(iElementX, index)[3] = -1. * UInnerPhysical[3];
                // }
            }
        }
    }
    
    for (unsigned jElementY = 0; jElementY < mesh.getNumberOfElementsY(); jElementY++)
    {
        for (unsigned index = 0; index <= polynomialOrder; index++)
        {
            // left
            {
                // outlet
                auto const jacobian {mesh.getJacobian(0, jElementY, -1., points[index])};
                
                auto const UInnerPhysical {transformValue<true>(innerFaceValues(0, jElementY, Face::Left, index), jacobian)};
                
                auto const pOut {outletPressureRatio * p0};
                
                double const rho = UInnerPhysical[1];
                
                vector2D const u {vector2D{UInnerPhysical[2] / rho, UInnerPhysical[3] / rho}};
                
                leftBorderFaceValues(jElementY, index)[1] = UInnerPhysical[1];
                leftBorderFaceValues(jElementY, index)[2] = UInnerPhysical[2];
                leftBorderFaceValues(jElementY, index)[3] = UInnerPhysical[3];
                leftBorderFaceValues(jElementY, index)[4] = thermo::getEnergy(rho, u, pOut);

                {
                    // inlet
                    
                    // auto const jacobian {mesh.getJacobian(0, jElementY, -1., points[index])};
                    
                    // auto const UInnerPhysical {transformValue<true>(innerFaceValues(0, jElementY, Face::Left, index), jacobian)};
                    
                    // double const pIn {std::min(thermo::getPressure(UInnerPhysical), p0)};
                    
                    // // double const mach {std::sqrt(2. / (thermo::kappa() - 1) * (std::pow(p0 / pIn, (thermo::kappa() - 1) / thermo::kappa()) - 1))};
                    
                    // // double rho {std::pow(pIn / p0, 1 / thermo::kappa()) * rho0};
                    
                    // // double const c {std::sqrt(thermo::kappa() * pIn / rho)};
                    
                    // // leftBorderFaceValues(jElementY, index)[1] = rho;
                    // // leftBorderFaceValues(jElementY, index)[2] = rho * mach * c;
                    // // leftBorderFaceValues(jElementY, index)[3] = 0.;
                    // // leftBorderFaceValues(jElementY, index)[4] = thermo::getEnergy(rho, vector2D{mach * c, 0.}, pIn);
                    
                    // leftBorderFaceValues(jElementY, index)[1] = 1.;
                    // leftBorderFaceValues(jElementY, index)[2] = 1.;
                    // leftBorderFaceValues(jElementY, index)[3] = 0.;
                    // leftBorderFaceValues(jElementY, index)[4] = pIn / (thermo::kappa() - 1) + 0.5;
                }
            }
            
            // right
            {
                // outlet
                auto const jacobian {mesh.getJacobian(mesh.getNumberOfElementsX() - 1, jElementY, 1., points[index])};
                
                auto const UInnerPhysical {transformValue<true>(innerFaceValues(mesh.getNumberOfElementsX() - 1, jElementY, Face::Right, index), jacobian)};
                
                // double const pOut {17.857};
                auto const pOut {outletPressureRatio * p0};
                
                double const rho = UInnerPhysical[1];
                
                vector2D const u {vector2D{UInnerPhysical[2] / rho, UInnerPhysical[3] / rho}};
                
                rightBorderFaceValues(jElementY, index)[1] = UInnerPhysical[1];
                rightBorderFaceValues(jElementY, index)[2] = UInnerPhysical[2];
                rightBorderFaceValues(jElementY, index)[3] = UInnerPhysical[3];
                rightBorderFaceValues(jElementY, index)[4] = thermo::getEnergy(rho, u, pOut);
            }
        }
    }
}

#endif
