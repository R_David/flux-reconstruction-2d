#ifndef FACE_VALUES_H
#define FACE_VALUES_H

#include <span>
#include <vector>

#include "utility.h"

template <typename Type, unsigned polynomialOrder>
class faceValues2D
{

public:
    
    faceValues2D (unsigned numberOfElementsX_, unsigned numberOfElementsY_) :
        numberOfElementsX (numberOfElementsX_),
        numberOfElementsY (numberOfElementsY_)
    {
        valuesX.resize(numberOfElementsX * (numberOfElementsY + 1) * (polynomialOrder + 1));
        valuesY.resize((numberOfElementsX + 1) * numberOfElementsY * (polynomialOrder + 1));
    };
    
    Type& operator() (unsigned iElementX, unsigned jElementY, Face facePosition, int index)
    {
        switch (facePosition)
        {
            case Face::Top:
                return valuesX[((jElementY + 1) * numberOfElementsX + iElementX) * (polynomialOrder + 1) + index];
            case Face::Bottom:
                return valuesX[(jElementY * numberOfElementsX + iElementX) * (polynomialOrder + 1) + index];
            case Face::Left:
                return valuesY[(iElementX * numberOfElementsY + jElementY) * (polynomialOrder + 1) + index];
            case Face::Right:
                return valuesY[((iElementX + 1) * numberOfElementsY + jElementY) * (polynomialOrder + 1) + index];
            default:
                return valuesX.front();
        }
    };
    
    Type const& operator() (unsigned iElementX, unsigned jElementY, Face facePosition, int index) const
    {
        switch (facePosition)
        {
            case Face::Top:
                return valuesX[((jElementY + 1) * numberOfElementsX + iElementX) * (polynomialOrder + 1) + index];
            case Face::Bottom:
                return valuesX[(jElementY * numberOfElementsX + iElementX) * (polynomialOrder + 1) + index];
            case Face::Left:
                return valuesY[(iElementX * numberOfElementsY + jElementY) * (polynomialOrder + 1) + index];
            case Face::Right:
                return valuesY[((iElementX + 1) * numberOfElementsY + jElementY) * (polynomialOrder + 1) + index];
            default:
                return valuesX.front();
        }
    };
    
    std::span<Type, polynomialOrder + 1> values1D (unsigned iElementX, unsigned jElementY, Face facePosition)
    {
        switch (facePosition)
        {
            case Face::Top:
            {
                unsigned startingIndex {((jElementY + 1) * numberOfElementsX + iElementX) * (polynomialOrder + 1)};
                return std::span<Type, polynomialOrder + 1> {&valuesX[startingIndex], polynomialOrder + 1};
            }   
            case Face::Bottom:
            {
                unsigned startingIndex {(jElementY * numberOfElementsX + iElementX) * (polynomialOrder + 1)};
                return std::span<Type, polynomialOrder + 1> {&valuesX[startingIndex], polynomialOrder + 1};
            }   
            case Face::Left:
            {
                unsigned startingIndex {(iElementX * numberOfElementsY + jElementY) * (polynomialOrder + 1)};
                return std::span<Type, polynomialOrder + 1> {&valuesY[startingIndex], polynomialOrder + 1};
            }
            case Face::Right:
            {
                unsigned startingIndex {((iElementX + 1) * numberOfElementsY + jElementY) * (polynomialOrder + 1)};
                return std::span<Type, polynomialOrder + 1> {&valuesY[startingIndex], polynomialOrder + 1};
            }
            default:
                return std::span<Type> {};
        }
    };
    
    std::span<Type const, polynomialOrder + 1> values1D (unsigned iElementX, unsigned jElementY, Face facePosition) const
    {
        switch (facePosition)
        {
            case Face::Top:
            {
                unsigned startingIndex {((jElementY + 1) * numberOfElementsX + iElementX) * (polynomialOrder + 1)};
                return std::span<Type const, polynomialOrder + 1> {&valuesX[startingIndex], polynomialOrder + 1};
            }   
            case Face::Bottom:
            {
                unsigned startingIndex {(jElementY * numberOfElementsX + iElementX) * (polynomialOrder + 1)};
                return std::span<Type const, polynomialOrder + 1> {&valuesX[startingIndex], polynomialOrder + 1};
            }   
            case Face::Left:
            {
                unsigned startingIndex {(iElementX * numberOfElementsY + jElementY) * (polynomialOrder + 1)};
                return std::span<Type const, polynomialOrder + 1> {&valuesY[startingIndex], polynomialOrder + 1};
            }
            case Face::Right:
            {
                unsigned startingIndex {((iElementX + 1) * numberOfElementsY + jElementY) * (polynomialOrder + 1)};
                return std::span<Type const, polynomialOrder + 1> {&valuesY[startingIndex], polynomialOrder + 1};
            }
            default:
                return std::span<Type const> {};
        }
    };
    
private:

    unsigned numberOfElementsX, numberOfElementsY;
    
    std::vector<Type> valuesX;
    std::vector<Type> valuesY;
};

#endif
