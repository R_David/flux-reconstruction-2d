#ifndef VISCOUS_FLUX_H
#define VISCOUS_FLUX_H

#include "myVector.h"

// input and output is in physical values
vector4D viscousFlux (vector4D const& ULeft, vector4D const& URight,
                      vector4D const& QLeftX, vector4D const& QLeftY,
                      vector4D const& QRightX, vector4D const& QRightY,
                      vector2D normalVector, bool output = false);

#endif
