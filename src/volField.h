#ifndef VOLFIELD_H
#define VOLFIELD_H

#include <vector>

template <typename Type>
class volField
{
public:
    
    volField (unsigned numberOfElementsX_, unsigned numberOfElementsY_) :
        numberOfElementsX (numberOfElementsX_),
        numberOfElementsY (numberOfElementsY_),
        numberOfElements (numberOfElementsX * numberOfElementsY)
    {
        values.resize(numberOfElementsX * numberOfElementsY);
    };
    
    Type& operator() (unsigned iElementX, unsigned jElementY)
    {
        return values[jElementY * numberOfElementsX + iElementX];
    };
    
    Type const& operator() (unsigned iElementX, unsigned jElementY) const
    {
        return values[jElementY * numberOfElementsX + iElementX];
    };
    
private:
    
    unsigned numberOfElementsX {};
    unsigned numberOfElementsY {};
    unsigned numberOfElements {};
    
    std::vector<Type> values;
};

#endif
