#ifndef POINT_VALUES_2D_H
#define POINT_VALUES_2D_H

#include <span>
#include <vector>

template <typename Type, unsigned polynomialOrder>
class pointValues2D
{
public:
    
    pointValues2D (unsigned numberOfElementsX_, unsigned numberOfElementsY_) :
        numberOfElements(numberOfElementsX_ * numberOfElementsY_),
        numberOfElementsX (numberOfElementsX_),
        numberOfElementsY (numberOfElementsY_)
    {
        values.resize(numberOfElements * (polynomialOrder + 1) * (polynomialOrder + 1));
    };
    
    Type& operator() (unsigned iElementX, unsigned jElementY, unsigned iPointX, unsigned jPointY)
    {
        return values[jElementY * numberOfElementsX * numberOfPointsInElement +
                      iElementX * numberOfPointsInElement + jPointY * (polynomialOrder + 1) + iPointX];
    };
    
    Type const& operator() (unsigned iElementX, unsigned jElementY, unsigned iPointX, unsigned jPointY) const
    {
        return values[jElementY * numberOfElementsX * numberOfPointsInElement +
                      iElementX * numberOfPointsInElement + jPointY * (polynomialOrder + 1) + iPointX];
    };
    
    std::span<Type, (polynomialOrder + 1) * (polynomialOrder + 1)> const elementPointValues (unsigned iElementX, unsigned jElementY)
    {
        return std::span<Type, (polynomialOrder + 1) * (polynomialOrder + 1)> (&values[jElementY * numberOfElementsX * numberOfPointsInElement + iElementX * numberOfPointsInElement],
                                                                               numberOfPointsInElement);
    };
    
    std::span<Type, polynomialOrder + 1> values1DHorizontal (unsigned iElementX, unsigned jElementY, unsigned index)
    {
        unsigned startingIndex {jElementY * numberOfElementsX * numberOfPointsInElement + iElementX * numberOfPointsInElement + index * (polynomialOrder + 1)};
        
        return std::span<Type, polynomialOrder + 1> {&values[startingIndex], polynomialOrder + 1};
    };
    
    std::span<Type const, polynomialOrder + 1> values1DHorizontal (unsigned iElementX, unsigned jElementY, unsigned index) const
    {
        unsigned startingIndex {jElementY * numberOfElementsX * numberOfPointsInElement + iElementX * numberOfPointsInElement + index * (polynomialOrder + 1)};
        
        return std::span<Type const, polynomialOrder + 1> {&values[startingIndex], polynomialOrder + 1};
    };
    
    std::array<Type, polynomialOrder + 1> const values1DVertical (unsigned iElementX, unsigned jElementY, unsigned index) const
    {        
        unsigned startingIndex {jElementY * numberOfElementsX * numberOfPointsInElement + iElementX * numberOfPointsInElement + index};
        
        std::array<Type, polynomialOrder + 1> result {};
        
        for (unsigned i = 0; i <= polynomialOrder; i++)
            result[i] = values[startingIndex + i * (polynomialOrder + 1)];
        
        return result;
    };
    
    static constexpr unsigned getNumberOfPoints (unsigned iElementX, unsigned jElementY)
    {
        return (polynomialOrder + 1) * (polynomialOrder + 1);
    };
    
    static constexpr unsigned getNumberOfPointsX ([[maybe_unused]] unsigned iElementX, [[maybe_unused]] unsigned jElementY)
    {
        return polynomialOrder + 1;
    };
    
    static constexpr unsigned getNumberOfPointsY ([[maybe_unused]] unsigned iElementX, [[maybe_unused]] unsigned jElementY)
    {
        return polynomialOrder + 1;
    };
    
    unsigned getNumberOfElementsX () const
    {
        return numberOfElementsX;
    };

    unsigned getNumberOfElementsY () const
    {
        return numberOfElementsY;
    };
    
    std::vector<Type>& getValues ()
    {
        return values;
    };
    
    std::vector<Type> const& getValues () const
    {
        return values;
    };
    
    Type const getValue (int index) const
    {
        return values[index];
    };
    
    Type& getValue (int index)
    {
        return values[index];
    };
    
private:
    
    unsigned numberOfElements, numberOfElementsX, numberOfElementsY;
    static constexpr unsigned numberOfPointsInElement {(polynomialOrder + 1) * (polynomialOrder + 1)};
    std::vector<Type> values;
};

template <typename Type, unsigned polynomialOrder>
pointValues2D<Type, polynomialOrder> operator+ (pointValues2D<Type, polynomialOrder> const& a, pointValues2D<Type, polynomialOrder> const& b)
{
    pointValues2D<Type, polynomialOrder> result(a.getNumberOfElementsX(), a.getNumberOfElementsY());;
    
    for (unsigned i {0}; i < a.getValues().size(); i++)
        result.getValue(i) = a.getValue(i) + b.getValue(i);
    
    return result;
}

template <typename Type, unsigned polynomialOrder>
pointValues2D<Type, polynomialOrder> operator- (pointValues2D<Type, polynomialOrder> const& a, pointValues2D<Type, polynomialOrder> const& b)
{
    pointValues2D<Type, polynomialOrder> result(a.getNumberOfElementsX(), a.getNumberOfElementsY());;
    
    for (unsigned i {0}; i < a.getValues().size(); i++)
        result.getValue(i) = a.getValue(i) - b.getValue(i);
    
    return result;
}

template <typename Type, unsigned polynomialOrder>
pointValues2D<Type, polynomialOrder> operator* (const double a, pointValues2D<Type, polynomialOrder> const& b)
{
    pointValues2D<Type, polynomialOrder> result(b.getNumberOfElementsX(), b.getNumberOfElementsY());;
    
    for (unsigned i {0}; i < b.getValues().size(); i++)
    {
        result.getValue(i) = b.getValue(i) * a;
    } 
    
    return result;
}

template <typename Type, unsigned polynomialOrder>
pointValues2D<Type, polynomialOrder> operator* (pointValues2D<Type, polynomialOrder> const& a, const double b)
{
    pointValues2D<Type, polynomialOrder> result(a.getNumberOfElementsX(), a.getNumberOfElementsY());;
    
    for (unsigned i {0}; i < a.getValues().size(); i++)
    {
        result.getValue(i) = a.getValue(i) * b;
    } 
    
    return result;
}

#endif
